# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170923162049) do

  create_table "appointment_types", force: :cascade do |t|
    t.string   "name",                      limit: 255,                null: false
    t.string   "slug",                      limit: 255,                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "questionnaire_description"
    t.string   "description"
    t.boolean  "is_active",                             default: true, null: false
  end

  add_index "appointment_types", ["is_active"], name: "index_appointment_types_on_is_active", using: :btree

  create_table "appointments", force: :cascade do |t|
    t.integer  "guide1_id",        null: false
    t.integer  "guide2_id"
    t.datetime "appointment_slot", null: false
    t.integer  "location_id",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appointments", ["guide1_id"], name: "index_appointments_on_guide1_id", using: :btree
  add_index "appointments", ["guide2_id"], name: "index_appointments_on_guide2_id", using: :btree
  add_index "appointments", ["location_id"], name: "index_appointments_on_location_id", using: :btree

  create_table "guides", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "is_admin",               default: false, null: false
    t.boolean  "must_change_password",   default: false, null: false
  end

  add_index "guides", ["email"], name: "index_guides_on_email", using: :btree
  add_index "guides", ["reset_password_token"], name: "index_guides_on_reset_password_token", unique: true, using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "name",        limit: 255,               null: false
    t.string   "description", limit: 1024,              null: false
    t.integer  "sort",                     default: 10
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "question_categories", force: :cascade do |t|
    t.string   "text",                null: false
    t.integer  "sort",                null: false
    t.integer  "appointment_type_id", null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "instructions"
  end

  add_index "question_categories", ["appointment_type_id"], name: "index_question_categories_on_appointment_type_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "text",                 null: false
    t.integer  "sort",                 null: false
    t.integer  "question_category_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "questions", ["question_category_id"], name: "index_questions_on_question_category_id", using: :btree

  create_table "registrations", force: :cascade do |t|
    t.string   "member_name",               null: false
    t.string   "member_email",              null: false
    t.string   "questionnaire_instance_id"
    t.datetime "questionnaire_done"
    t.datetime "reminder_email_sent"
    t.integer  "appointment_type_id",       null: false
    t.integer  "appointment_id",            null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "registrations", ["appointment_id"], name: "index_registrations_on_appointment_id", using: :btree
  add_index "registrations", ["appointment_type_id"], name: "index_registrations_on_appointment_type_id", using: :btree

  create_table "settings", force: :cascade do |t|
    t.string   "key"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "settings", ["key"], name: "index_settings_on_key", unique: true, using: :btree

  add_foreign_key "question_categories", "appointment_types"
  add_foreign_key "questions", "question_categories"
  add_foreign_key "registrations", "appointment_types"
  add_foreign_key "registrations", "appointments"
end
