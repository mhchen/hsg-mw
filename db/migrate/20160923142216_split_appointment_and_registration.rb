class SplitAppointmentAndRegistration < ActiveRecord::Migration[4.2]
  def up
    Appointment.all.each do |a|
      next if a.read_attribute('member_name').blank? || a.read_attribute('member_email').blank? || a.read_attribute('appointment_type_id').blank?
      a.create_registration!(
        member_name: a.read_attribute('member_name'),
        member_email: a.read_attribute('member_email'),
        appointment_type_id: a.read_attribute('appointment_type_id'),
        questionnaire_done: a.survey_done ? Time.at(0) : nil,
        reminder_email_sent: a.read_attribute('reminder_email_sent') ? Time.at(0) : nil,
        questionnaire_instance_id: a.read_attribute('questionnaire_instance_id'),
      )
    end
    remove_column :appointments, :member_email
    remove_column :appointments, :member_name
    remove_column :appointments, :appointment_type_id
    remove_column :appointments, :survey_done
    remove_column :appointments, :reminder_email_sent
    remove_column :appointments, :questionnaire_instance_id
  end

  def down
    add_column :appointments, :member_email, :string
    add_column :appointments, :member_name, :string
    add_column :appointments, :appointment_type_id, :integer
    add_column :appointments, :survey_done, :boolean
    add_column :appointments, :reminder_email_sent, :boolean
    add_column :appointments, :questionnaire_instance_id, :string
  end
end
