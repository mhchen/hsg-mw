class MakeGuide2Optional < ActiveRecord::Migration[4.2]
  def change
    change_column :appointments, :guide2_id, :integer, null: true
  end
end
