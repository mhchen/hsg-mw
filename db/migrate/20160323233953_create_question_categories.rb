class CreateQuestionCategories < ActiveRecord::Migration[4.2]
  def change
    create_table :question_categories do |t|
      t.string :text, null: false
      t.integer :sort, null: false
      t.references :questionnaire, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
