class RenameQuestionnairesToMeetingTypes < ActiveRecord::Migration[4.2]
  def change
    rename_table :questionnaires, :meeting_types
    rename_column :appointments, :questionnaire_id, :meeting_type_id
    rename_column :meeting_types, :description, :questionnaire_description
    rename_column :meeting_types, :meeting_description, :description
  end
end
