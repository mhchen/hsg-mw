class AddInfoToQuestionCategories < ActiveRecord::Migration[4.2]
  def change
    add_column :question_categories, :instructions, :string
  end
end
