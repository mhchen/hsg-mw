class ChangeNotNullColumnsInAppointments < ActiveRecord::Migration[4.2]
  def change
    change_column_null :appointments, :appointment_slot, false
    change_column_null :appointments, :location_id, false
  end
end
