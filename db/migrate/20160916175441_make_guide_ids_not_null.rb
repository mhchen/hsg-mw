class MakeGuideIdsNotNull < ActiveRecord::Migration[4.2]
  def up
    Appointment.where({guide1_id: nil}).delete_all
    Appointment.where({guide2_id: nil}).delete_all
    change_column :appointments, :guide1_id, :integer, null: false
    change_column :appointments, :guide2_id, :integer, null: false
  end

  def down
    change_column :appointments, :guide1_id, :integer
    change_column :appointments, :guide2_id, :integer
  end
end
