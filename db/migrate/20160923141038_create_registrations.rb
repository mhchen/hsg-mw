class CreateRegistrations < ActiveRecord::Migration[4.2]
  def change
    create_table :registrations do |t|
      t.string :member_name, null: false
      t.string :member_email, null: false
      t.string :questionnaire_instance_id, null: true
      t.datetime :questionnaire_done, null: true
      t.datetime :reminder_email_sent, null: true
      t.references :appointment_type, index: true, foreign_key: true, null: false
      t.references :appointment, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
