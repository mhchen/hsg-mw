class MakeReminderEmailSentNotNull < ActiveRecord::Migration[4.2]
  def up
    change_column(:appointments, :reminder_email_sent, :boolean, null: false, default: false)
  end

  def down
    change_column(:appointments, :reminder_email_sent, :boolean, null: true, default: false)
  end
end
