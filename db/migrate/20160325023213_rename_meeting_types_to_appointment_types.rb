class RenameMeetingTypesToAppointmentTypes < ActiveRecord::Migration[4.2]
  def change
    rename_table :meeting_types, :appointment_types
    rename_column :appointments, :meeting_type_id, :appointment_type_id
    rename_column :question_categories, :meeting_type_id, :appointment_type_id
  end
end
