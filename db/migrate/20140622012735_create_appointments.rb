class CreateAppointments < ActiveRecord::Migration[4.2]
  def change
    create_table :appointments do |t|
      t.integer :guide1_id
      t.integer :guide2_id
      t.datetime :appointment_slot, null: false
      t.references :location, index: true
      t.string :member_name
      t.string :member_email
      t.references :questionnaire, index: true
      t.boolean :survey_done, null: false, default: false
      t.boolean :reminder_email_sent, null: false, default: false

      t.timestamps
    end
    add_index :appointments, :guide1_id
    add_index :appointments, :guide2_id
  end
end
