class RenameQuestionnaireIdToMeetingTypeId < ActiveRecord::Migration[4.2]
  def change
    rename_column :question_categories, :questionnaire_id, :meeting_type_id
  end
end
