class AddIsAdminToGuides < ActiveRecord::Migration[4.2]
  def change
    add_column :guides, :is_admin, :boolean, null: false, default: false
  end
end
