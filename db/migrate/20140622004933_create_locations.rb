class CreateLocations < ActiveRecord::Migration[4.2]
  def change
    create_table :locations do |t|
      t.string :name, limit: 255, null: false
      t.string :description, limit: 1024, null: false
      t.integer :sort, default: 10

      t.timestamps
    end
  end
end
