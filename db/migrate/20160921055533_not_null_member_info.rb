class NotNullMemberInfo < ActiveRecord::Migration[4.2]
  def up
    change_column :appointments, :member_name, :string, null: false, default: ''
    change_column :appointments, :member_email, :string, null: false, default: ''
  end

  def down
    change_column :appointments, :member_name
    change_column :appointments, :member_email
  end
end
