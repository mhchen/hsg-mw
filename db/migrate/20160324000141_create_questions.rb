class CreateQuestions < ActiveRecord::Migration[4.2]
  def change
    create_table :questions do |t|
      t.string :text, null: false
      t.integer :sort, null: false
      t.references :question_category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
