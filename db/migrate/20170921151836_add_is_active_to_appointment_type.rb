class AddIsActiveToAppointmentType < ActiveRecord::Migration[4.2]
  def change
    add_column :appointment_types, :is_active, :boolean, null: false, default: true
    add_index :appointment_types, :is_active
  end
end
