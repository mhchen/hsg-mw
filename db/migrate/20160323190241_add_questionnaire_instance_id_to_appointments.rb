class AddQuestionnaireInstanceIdToAppointments < ActiveRecord::Migration[4.2]
  def change
    add_column :appointments, :questionnaire_instance_id, :string
  end
end
