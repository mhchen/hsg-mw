class AddMeetingDescriptionToQuestionnaire < ActiveRecord::Migration[4.2]
  def change
    add_column :questionnaires, :meeting_description, :string
  end
end
