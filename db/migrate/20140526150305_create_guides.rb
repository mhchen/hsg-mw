class CreateGuides < ActiveRecord::Migration[4.2]
  def change
    create_table :guides do |t|
      t.string :first_name
      t.string :last_name
      t.string :email

      t.timestamps
    end

    add_index :guides, :email
  end
end
