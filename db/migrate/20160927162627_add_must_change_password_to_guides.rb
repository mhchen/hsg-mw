class AddMustChangePasswordToGuides < ActiveRecord::Migration[4.2]
  def change
    add_column :guides, :must_change_password, :boolean, null: false, default: false
  end
end
