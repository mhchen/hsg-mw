class CreateQuestionnaires < ActiveRecord::Migration[4.2]
  def change
    create_table :questionnaires do |t|
      t.string :name, limit: 255, null: false
      t.string :slug, limit: 255, null: false

      t.timestamps
    end
  end
end
