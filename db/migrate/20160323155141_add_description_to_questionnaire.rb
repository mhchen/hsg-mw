class AddDescriptionToQuestionnaire < ActiveRecord::Migration[4.2]
  def change
    add_column :questionnaires, :description, :string
  end
end
