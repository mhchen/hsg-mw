# Usage: rake db:seed [real=[yes|only]]
#
# The real environment parameter allows the generation of guides that have real
# emails for testing purposes. The "only" parameter means fake guides will not
# be generated alongside the real guides.

AppointmentType.create!([
  {name: "New member", slug: "new-member", questionnaire_description: "the new member questionnaire", description: "new member Highrock Spiritual Guides appointment"},
  {name: "Membership renewal", slug: "renewal", questionnaire_description: "the member renewal questionnaire", description: "Highrock Spiritual Guides membership renewal appointment"},
  {name: "Spiritual Guides", slug: "spiritual-guides", questionnaire_description: "a brief survey to prepare for your appointment", description: "Highrock Spiritual Guides meeting"}
])

Location.create!([
  {name: "Highrock Spiritual Guides Room (Mill St Meeting Room A)", description: "Mill Street, the first room on your left after going through the single door to the hallway", sort: 10},
  {name: "Mill St Meeting Room B", description: "Mill Street, far end of the hallway next to the kitchen with the very large window.", sort: 10},
  {name: "Pastor Eugene's Office", description: "Mill Street, third door on your right after entering the building", sort: 10},
  {name: "Shawn Houten's Office", description: "Mill Street, first door on your right after entering the building", sort: 10}
])

QuestionCategory.create!([
  {text: "Connecting to God Personally", sort: 10, appointment_type_id: 1},
  {text: "Connecting to God's People", sort: 20, appointment_type_id: 1},
  {text: "Connecting to God's Purposes", sort: 30, appointment_type_id: 1},
  {text: "Summary", sort: 40, appointment_type_id: 1},
  {text: "Connecting to God Personally", sort: 10, appointment_type_id: 2},
  {text: "Connecting to God's People", sort: 20, appointment_type_id: 2},
  {text: "Connecting to God's Purposes", sort: 30, appointment_type_id: 2},
  {text: "Summary", sort: 40, appointment_type_id: 2},
  {text: "Questions", sort: 5, appointment_type_id: 3},
])


Question.create!([
  {text: "Please share your journey of coming to faith in Jesus.", sort: 1, question_category_id: 1},
  {text: "Where do you connect with or struggle to connect with God (e.g., worship service, small group, Bible reading, prayer, other)?", sort: 2, question_category_id: 1},
  {text: "Are there any areas of struggle you wish to discuss?", sort: 3, question_category_id: 1},
  {text: "Where and how are you connecting with God’s people? Or where do you have difficulties connecting with others?", sort: 5, question_category_id: 2},
  {text: "Are there any broken relationships in your life that invite repentance, forgiveness, or reconciliation?  Explain.", sort: 6, question_category_id: 2},
  {text: "Tell us about your efforts to practice faithful stewardship with your finances, talents, and time to pursue God's purposes in this world. ", sort: 8, question_category_id: 3},
  {text: "How have you been sharing your spiritual journey or witnessing with others? ", sort: 9, question_category_id: 3},
  {text: "Are you involved in any ministries at Highrock? Tell us about your experience and what you are learning about your gifts.", sort: 10, question_category_id: 3},
  {text: "How do you sense that you are growing in your love for God and neighbor this year?", sort: 12, question_category_id: 4},
  {text: "Is there anything else you would like to share with us about your spiritual journey or your experience at Highrock? ", sort: 13, question_category_id: 4},
  {text: "Do you have any questions about Highrock or are there ministries that you would like to be connected with? ", sort: 14, question_category_id: 4},

  {text: "Where do you connect with or struggle to connect with God (e.g., worship service, small group, Bible reading, prayer, other)?", sort: 2, question_category_id: 5},
  {text: "Are there any areas of struggle you wish to discuss?", sort: 3, question_category_id: 5},
  {text: "Where and how are you connecting with God’s people? Or where do you have difficulties connecting with others?", sort: 5, question_category_id: 6},
  {text: "Are there any broken relationships in your life that invite repentance, forgiveness, or reconciliation?  Explain.", sort: 6, question_category_id: 6},
  {text: "Tell us about your efforts to practice faithful stewardship with your finances, talents, and time to pursue God's purposes in this world. ", sort: 8, question_category_id: 7},
  {text: "How have you been sharing your spiritual journey or witnessing with others? ", sort: 9, question_category_id: 7},
  {text: "Are you involved in any ministries at Highrock? Tell us about your experience and what you are learning about your gifts.", sort: 10, question_category_id: 7},
  {text: "How do you sense that you are growing in your love for God and neighbor this year?", sort: 12, question_category_id: 8},
  {text: "Is there anything else you would like to share with us about your spiritual journey or your experience at Highrock? ", sort: 13, question_category_id: 8},

  {text: "What are you seeking from God for your life?", sort: 1, question_category_id: 9},
  {text: "What is happening in your prayer life?", sort: 2, question_category_id: 9},
  {text: "How has your image of God changed over the years?  Who is God to you today?", sort: 3, question_category_id: 9},
  {text: "Where is it easiest (and/or most difficult) for you to connect with God?" , sort: 4, question_category_id: 9},
  {text: "Do you sense that God is inviting you into something new or different?", sort: 5, question_category_id: 9},
  {text: "Are you facing a choice that requires spiritual discernment?  How are you listening to God about your choice?", sort: 6, question_category_id: 9},
  {text: "What spiritual practices sustain you on a daily or monthly basis?" , sort: 7, question_category_id: 9},
])

if Rails.env.development?
  def create_user
    first = Faker::Name.first_name
    {
      first_name: first,
      last_name: Faker::Name.last_name,
      email: Faker::Internet.safe_email(first),
      password: 'abcdef'
    }
  end

  Guide.create!({
    first_name: 'Joe',
    last_name: 'Nonadmin',
    email: 'joe@highrock.org',
    password: 'abcdef'
  })
  Guide.create!({
    first_name: 'Jim',
    last_name: 'Nonadmin',
    email: 'jim@highrock.org',
    password: 'abcdef'
  })
  Guide.create!({
    first_name: 'Carla',
    last_name: 'Admin',
    email: 'admin@highrock.org',
    is_admin: true,
    password: 'abcdef'
  })


  50.times.each do
    appointment = Appointment.new
    guide1_id = 1 + rand(Guide.count)
    guide2_id = guide1_id
    until guide2_id != guide1_id
      guide2_id = 1 + rand(Guide.count)
    end
    slot = rand(4) == 0 ? Faker::Time.backward(60) : Faker::Time.forward(60)
    slot = slot.change({min: [0, 15, 30, 45].sample})
    signed_up = [true, false].sample

    appointment.guide1_id = guide1_id
    appointment.guide2_id = guide2_id
    appointment.appointment_slot = slot
    appointment.location_id = 1 + rand(Location.count)
    appointment.save!
    if signed_up
      member_name = Faker::Name.name
      appointment.create_registration(
        member_name: member_name,
        member_email: Faker::Internet.safe_email(member_name),
        appointment_type_id: 1 + rand(AppointmentType.count)
      )
    end
  end
end
