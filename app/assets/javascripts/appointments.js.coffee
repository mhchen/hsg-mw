$modal = $('#js-modal-form')
$modalDialog = $modal.find('.modal-dialog')

$('.js-sign-up').click ->
  $this = $(@)
  $modalDialog.load("/appointments/#{$this.data('id')}/edit", ->
    $modal.modal('show')
  )

# If the modal is populated on creation, it's a filled-out form. Show it immediately.
if $modalDialog.find('.modal-content').length
  $modal.modal('show')

# Focus first field on modal open.
$modal.on 'shown.bs.modal', ->
  $modal.find('input:not([type="hidden"])').first().focus()
