'use strict'

ACTION_BTNS_HTML = $('#js-action-btns').html()

(->
  $table = $('#js-guides')

  return unless $table.length

  $modal = $('#js-modal')
  $deleteModal = $('#js-delete-modal')

  dt = $table.DataTable
    data: window.guides
    createdRow: (row, data) ->
      $(row).attr('data-id', data.id)
        .data('name', "#{data.first_name} #{data.last_name}")
    columns: [{
      data: 'first_name'
      title: 'First name'
    }, {
      data: 'last_name'
      title: 'Last name'
    }, {
      data: 'email'
      title: 'Email'
    }, {
      className: 'actions'
      searchable: false
      sortable: false
      data: ->
        ACTION_BTNS_HTML
    }]

  # New guide button.
  $('.js-add-row').click ->
    $modal.find('.modal-dialog').load('/admin/guides/new', ->
      $modal.modal('show')
    )

  # Edit guide button.
  $table.on('click', '.js-edit', (e) ->
    id = $(e.currentTarget).closest('tr').data('id')

    $modal.find('.modal-dialog').load("/admin/guides/#{id}/edit", ->
      $modal.modal('show')
    )
  )

  $modal.on('submit', 'form', (e) ->
    # Send the form data to the server
    e.preventDefault()
    $form = $(@)
    $.ajax
      type: $form.attr('method')
      url: $form.attr('action')
      data: $form.serialize()
      success: (res) ->
        $modal.modal('hide')
        if /^edit_/.test($form.attr('id'))
          $row = $table.find("[data-id=#{res.id}]")
          dt.row($row).data(res).draw(false)
          toastr.success('Guide successfully updated')
        else
          dt.row.add(res).draw(false)
          toastr.success('Guide successfully created')
  )

  # Delete behavior.
  $table.on('click', '.js-delete', (e) ->
    $row = $(e.currentTarget).closest('tr')
    $deleteModal.find('#js-delete-name').text($row.data('name'))
    $deleteModal.find('#js-delete-modal-confirm').off('click').click( ->
      id = $row.data('id')
      $.ajax("/admin/api/guides/#{id}", {
        type: 'DELETE'
        success: ->
          $deleteModal.modal('hide')
          toastr.success('Guide deleted')
          dt.row($row).remove().draw()
      })
    )
    $deleteModal.modal('show')
  )
)()
