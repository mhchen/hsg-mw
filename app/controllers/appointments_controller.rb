class AppointmentsController < ApplicationController
  layout false, except: [:index, :thank_you]

  def index
    @appointments = Appointment.filter_for_public.unregistered.ordered.having_associations

    # Fetch the currently-edited appointment, if any.
    if flash[:registration] && !flash[:conflict]
      @current_appointment = Appointment.find(flash[:appointment_id])
      unless @current_appointment.registration
        @current_appointment.build_registration(flash[:registration])
        @current_appointment.validate
      end
    end
  end

  def edit
    @appointment = Appointment.find(params[:id]) or not_found
    return head :conflict if @appointment.registration
    @appointment.build_registration
  end

  def update
    @appointment = Appointment.find(params[:id]) or not_found
    if @appointment.registration
      flash[:conflict] = true
    else
      @appointment.build_registration(
        appointment_params[:registration_attributes]
      )
      if @appointment.registration.save
        GuidesMailer.registration_email(@appointment).deliver_now
        return redirect_to action: 'thank_you'
      end
    end

    flash[:appointment_id] = @appointment.id
    flash[:registration] = appointment_params[:registration_attributes]

    redirect_to action: 'index'
  end

  def thank_you
  end

  private
  def appointment_params
    params.require(:appointment).permit(registration_attributes: [
      :member_name, 
      :member_email, 
      :member_email_confirmation, 
      :appointment_type_id
    ])
  end
end
