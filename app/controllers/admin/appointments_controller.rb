require 'csv'

class Admin::AppointmentsController < Admin::AdminController
  before_action :authenticate_guide!
  layout false, except: :index

  def index
    @guides = Guide.all

    @dates_for_filter = {
      'Starting today' => 'today',
      'All dates' => 'all',
    }
  end

  def new
    @appointment = Appointment.new
  end

  def edit
    @appointment = Appointment.find(params[:id])
  end

  def csv
    csv_string = CSV.generate do |csv|
      header_row = [
        'Time slot',
        'Guide 1',
        'Guide 2',
      ]
      if current_guide.is_admin
        header_row += [
          'Location',
          'Member name',
          'Member email',
        ]
      end
      header_row += ['Appointment type']
      csv << header_row
      Appointment.from_params(params).ordered.each do |appointment|
        row = [
          appointment.appointment_slot.in_time_zone,
          appointment.guide1.try(:name),
          appointment.guide2.try(:name),
        ]
        if current_guide.is_admin
          row += [
            appointment.location.try(:name),
            appointment.member_name,
            appointment.member_email,
          ]
        end
        row += [appointment.appointment_type.try(:name)]
        csv << row
      end
    end
    send_data csv_string, type: 'text/csv; charset=utf-8; header=present', disposition: 'attachment;filename=appointments.csv'
  end
end
