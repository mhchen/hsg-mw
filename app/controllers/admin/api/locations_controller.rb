class Admin::Api::LocationsController < Admin::AdminController
  before_action :authenticate_guide!
  respond_to :json

  def index
    render json: Location.all
  end

  def mass_update
    if params[:locations]
      Location.transaction do
        params[:locations].each do |location|
          id = location.delete('id')
          location_params = location.permit(:name, :description)
          if id
            if location['_delete']
              Location.delete(id)
            else
              Location.update(id, location_params)
            end
          else
            Location.create(location_params)
          end
        end
      end
    end
    head :no_content
  end

  def count_dependent_appointments
    render plain: Appointment.future_only.where(location_id: params[:location_ids]).count
  end
end
