class Admin::Api::Guides::PasswordsController < Admin::AdminController
  def reset_password
    guide = Guide.find_by(email: params[:email]) or not_found
    guide.send_reset_password_instructions
    head :no_content
  end

  def from_token
    guide = Guide.with_reset_password_token(params[:token]) or not_found
    render json: {
      guide: guide,
      last_sign_in_at: guide.last_sign_in_at
    }
  end

  def reset_password
    guide = Guide.find_by(email: params[:email]) or not_found
    guide.send_reset_password_instructions
    head :no_content
  end

  def new_password
    was_signed_in = guide_signed_in?
    guide = current_guide
    guide = Guide.with_reset_password_token(params[:token]) unless guide

    if guide && guide.reset_password(params[:password], params[:password_confirm])
      guide.update!(must_change_password: false)
      if was_signed_in
        bypass_sign_in(guide, scope: :guide)
      else
        sign_in(:guide, guide)
      end
      return head :no_content
    end
    head :bad_request
  end
end
