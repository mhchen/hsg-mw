class Admin::Api::Guides::SessionsController < Devise::SessionsController
  before_action :configure_sign_in_params, only: [:create]

  def create
    guide = Guide.find_for_database_authentication(email: params[:email])
    return raise_invalid_attempt unless guide
    if guide.valid_password?(params[:password])
      sign_in :guide, guide
      return render json: guide
    end

    raise_invalid_attempt
  end

  def destroy
    super
  end

  def current_user
    render json: current_guide
  end

  protected

  def raise_invalid_attempt
    render json: {
      error: 'Invalid email or password'
    }, status: :unauthorized
  end

  def configure_sign_in_params
    devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  end
end
