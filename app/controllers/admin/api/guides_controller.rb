class Admin::Api::GuidesController < Admin::AdminController
  before_action :authenticate_guide!
  before_action :ensure_admin, except: [:update_me, :index]
  respond_to :json, :js

  def index
    render json: Guide.exclude_superadmin.all
  end

  def update
    guide = Guide.find(params[:id])
    if guide.update_attributes(guide_params)
      render json: guide
    else
      render json: guide.errors.details, status: :unprocessable_entity
    end
  end

  def destroy
    render json: Guide.destroy(params[:id])
  end

  def create
    no_email = params['no_email']
    guide = Guide.create_with_initial_password(guide_params)
    if guide.persisted?
      GuidesMailer.welcome_email(guide).deliver_now unless no_email
      render json: guide
    else
      render json: guide.errors.details, status: :unprocessable_entity
    end
  end

  def update_me
    if current_guide.update_attributes(guide_params)
      render json: current_guide
    else
      render json: current_guide.errors.details, status: :unprocessable_entity
    end
  end

  def count_dependent_appointments
    render plain: Appointment.future_only.filter_by_guide(params[:id]).count
  end

  private

  def guide_params
    params.permit(:first_name, :last_name, :email, :is_admin)
  end
end
