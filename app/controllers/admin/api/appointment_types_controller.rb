class Admin::Api::AppointmentTypesController < Admin::AdminController
  before_action :authenticate_guide!
  respond_to :json

  def index
    render json: AppointmentType.all
  end

  def show
    appointment_type = AppointmentType.include_grouped_questions.find(params[:id])
    render json: appointment_type.to_json(include: {
      question_categories: {
        include: 'questions'
      }
    })
  end

  def update
    Question.transaction do
      strong_params[:question_categories].each do |params_category|
        next unless params_category[:questions]
        category = QuestionCategory.find(params_category[:id])
        category.update_attributes!(questions_attributes: params_category[:questions])
      end
    end
    head :no_content
  end

  private
  def strong_params()
    params.permit(question_categories: [
      :id, {
      questions: [:id, :text, :sort, :_destroy]
    }])
  end
end
