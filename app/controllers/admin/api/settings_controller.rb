class Admin::Api::SettingsController < Admin::AdminController
  before_action :ensure_admin

  def index
    render json: Setting.as_hash
  end
  
  def mass_update
    Setting.mass_update(params[:settings])
    head :no_content
  end
end
