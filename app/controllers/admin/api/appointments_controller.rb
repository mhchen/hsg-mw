class Admin::Api::AppointmentsController < Admin::AdminController
  before_action :authenticate_guide!

  def index
    render json: Appointment.get_dt_data(params, current_guide)
  end

  def calendar
    render json: Appointment.get_calendar_data(params, current_guide)
  end

  def show
    appointment = Appointment.includes_registration.find(params[:id])
    if current_guide.can_edit?(appointment)
      render json: appointment.as_json(include: :registration)
    else
      head :unauthorized
    end
  end

  def update
    appointment = Appointment.find(params[:id])
    return render(json: {
      error: 'You have to select yourself as one of the guides for this appointment'
    }, status: :unauthorized) unless current_guide.can_edit?(appointment)

    if appointment.update(appointment_params)
      if appointment.registration && params['send_confirmation_email']
        GuidesMailer.registration_email(appointment).deliver_now
      end
      render json: appointment
    else
      render json: appointment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    appointment = Appointment.find(params[:id])

    return head :unauthorized unless current_guide.can_edit?(appointment)
    if !appointment.member_email.blank? && !params[:no_send_email]
      GuidesMailer.cancellation_email(appointment).deliver_now
    end
    appointment.destroy!
    head :ok
  end

  def create
    appointment = Appointment.new(appointment_params)

    return render(json: {
      error: 'You have to select yourself as one of the guides for this appointment'
    }, status: :unauthorized) unless current_guide.can_edit?(appointment)

    appointment.assign_location
    if appointment.save
      if appointment.registration && params['send_confirmation_email']
        GuidesMailer.registration_email(appointment).deliver_now
      end
      render json: appointment
    else
      render json: appointment.errors, status: :unprocessable_entity
    end
  end

  private

  def appointment_params
    # Convert time to timezoned time.
    params['appointment_slot_time'] = params['appointment_slot_time'].in_time_zone(Time.zone)

    params.permit(
      :appointment_slot_date,
      :appointment_slot_time,
      :guide1_id,
      :guide2_id,
      :location_id,
      registration_attributes: [:id, :member_name, :member_email, :appointment_type_id, :_destroy]
    )
  end
end
