class Admin::AdminController < ApplicationController
  layout 'admin'

  def index
    render html: '', layout: true
  end

  private
  def ensure_admin
    return head(:unauthorized) unless current_guide && current_guide.is_admin
  end
end
