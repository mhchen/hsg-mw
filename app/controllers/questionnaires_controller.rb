class QuestionnairesController < ApplicationController
  def show
    @registration = Registration.find_by(questionnaire_instance_id: params[:instance_id]) or not_found
    @appointment = @registration.appointment

    @questionnaire = Questionnaire.new(@appointment)
  end

  def respond
    @appointment = Appointment.find_by_questionnaire_instance_id(params[:instance_id]) or not_found

    @questionnaire = Questionnaire.new(@appointment, params[:questionnaire])
    if @questionnaire.valid?
      @appointment.update_questionnaire_done(Time.zone.now)
      GuidesMailer.questionnaire_email(@questionnaire).deliver_now
      return redirect_to action: :thank_you
    end
    render 'show'
  end

  def thank_you
  end
end
