module ApplicationHelper
  def gulp_asset_path(path)
    path = REV_MANIFEST[path] if defined?(REV_MANIFEST) && REV_MANIFEST[path] && ['staging', 'production'].include?(Rails.env)
    "/assets/#{path}"
  end

  def get_setting(setting)
    Setting.send(setting)
  end
end
