class Guide < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :email, presence: true, uniqueness: true
  validates :first_name, :last_name, presence: true

  default_scope { order(first_name: :asc, last_name: :asc) }

  scope :exclude_superadmin, -> { where.not(email: 'admin@admin.admin') }

  scope :dt_filter, -> (params) {
    offset(params[:start]).limit(params[:length])
  }

  def self.create_with_initial_password(params)
    password = Devise.friendly_token.first(8)
    Guide.create(params.merge(
      password: password,
      must_change_password: true,
    ))
  end

  def name
    "#{first_name} #{last_name}"
  end

  def display_email
    "#{name} <#{email}>"
  end

  def is_guide_for?(appointment)
    appointment.guide1_id == id || appointment.guide2_id == id
  end

  def can_edit?(appointment)
    is_admin || is_guide_for?(appointment)
  end
end
