class Question < ActiveRecord::Base
  belongs_to :question_category
  validates_presence_of :text

  default_scope { order(:sort) }

  before_create :set_sort_value

  private

  # Defaults sort value to max + 10
  def set_sort_value
    return if self.sort
    current_max = Question.where(
      question_category_id: self.question_category_id
    ).maximum(:sort) || 0

    self.sort = current_max + 10
  end

end
