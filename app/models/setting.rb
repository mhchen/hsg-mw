class Setting < ActiveRecord::Base
  KEYS = [
    'admin_email',
    'director_email',
    'director_name',
    'director_title',
    'church_website_url',
    'church_name',
    'email_closing',
    'prepare_url',
    'ministry_description_url',
  ]

  def self.as_hash
    all.inject({}) do |hash, setting|
      hash[setting.key] = setting.value
      hash
    end.slice(*KEYS)
  end

  def self.mass_update(hash)
    KEYS.each do |key|
      val = hash[key] || hash[key.to_sym]
      self.send(:"#{key}=", val)
    end
  end

  KEYS.each do |key|
    define_singleton_method key do
      find_by(key: key).try(:value)
    end

    define_singleton_method "#{key}=" do |val|
      setting = Setting.find_or_initialize_by(key: key)
      setting.value = val if val
      setting.save
    end
  end
end
