class AppointmentType < ActiveRecord::Base
  has_many :question_categories

  # Sort by ID. This is terrible but this table changes really infrequently.
  default_scope { order('appointment_types.id ASC').where(is_active: true) }

  scope :include_grouped_questions, -> {
    includes(question_categories: :questions)
    .order('question_categories.sort ASC', 'questions.sort ASC')
  }
end
