class Registration < ActiveRecord::Base
  belongs_to :appointment_type
  belongs_to :appointment

  validates :member_name, presence: true

  validates :member_email, format: {
    with: Devise.email_regexp,
    message: 'Please enter a valid email'
  }, presence: true
  validates :member_email, confirmation: {
    message: 'Must match email'
  }
  validates :appointment_type, presence: true

  before_create :generate_questionnaire_instance_id
  # HACK ALERT: It sucks to not have this but Rails is pretty broken with this validation.
  # If the parent appointment is saved first this validation will fail.
  #validates :appointment, presence: true, uniqueness: true

  def generate_questionnaire_instance_id
    return unless questionnaire_instance_id.blank?
    begin
      id = SecureRandom.urlsafe_base64(8)
      self.questionnaire_instance_id = id
    end while self.class.exists?(questionnaire_instance_id: id)
  end

  def display_email
    "#{member_name} <#{member_email}>"
  end
end
