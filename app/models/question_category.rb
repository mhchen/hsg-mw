class QuestionCategory < ActiveRecord::Base
  belongs_to :appointment_type
  has_many :questions

  validates :appointment_type, :text, presence: true
  validates :text, uniqueness: {scope: [:appointment_type_id]}
  accepts_nested_attributes_for :questions, allow_destroy: true

  default_scope { order(:sort) }
end
