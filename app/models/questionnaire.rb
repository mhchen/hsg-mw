class Questionnaire 
  # TODO: determine if this is really necessary for use in simple_form. It's probably not.
  include ActiveAttr::Model

  attr_accessor :address, :phone, :highrock_campus
  attr_reader :categories, :field_output_order, :appointment

  def initialize(appointment, attribs = {})
    @appointment = appointment

    @field_output_order = [:address, :phone, :highrock_campus]

    @categories = appointment.appointment_type.question_categories
    @categories.each do |category|
      category.questions.each do |question|
        singleton_class.class_eval { attr_accessor "question_#{question.id}" }
      end
    end

    super(attribs)
  end

  def guides_appointment?
    @appointment.appointment_type.slug == 'spiritual-guides'
  end
end
