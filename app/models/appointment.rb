class Appointment < ActiveRecord::Base
  extend TimeSplitter::Accessors
  split_accessor :appointment_slot, date_format: '%m/%d/%Y', time_format: '%l:%M%P', default: -> {Time.zone.now}

  # Duration of meetings for location auto-assign purposes.
  MEETING_LENGTH = 90.minutes

  validates :guide1, :location, :appointment_slot, presence: true

  belongs_to :location
  belongs_to :guide1, class_name: 'Guide', foreign_key: 'guide1_id'
  belongs_to :guide2, class_name: 'Guide', foreign_key: 'guide2_id'
  has_one :registration, dependent: :destroy
  accepts_nested_attributes_for :registration, allow_destroy: true

  delegate :appointment_type, :member_name, :member_email, :questionnaire_instance_id, to: :registration, allow_nil: true

  scope :with_associations, -> { 
    includes(:guide1)
    .includes(:guide2)
    .includes(:location)
    .includes(registration: :appointment_type)
    .references(:registrations)
  }

  scope :includes_registration, -> { includes(:registration) }

  scope :filter_for_public, -> {
    where("appointment_slot > ?", Time.zone.now + 4.days)
  }

  scope :future_only, -> {
    where("appointment_slot > ?", Time.zone.now.at_midnight)
  }

  scope :filter_by_start_date_str, -> (start_date_str) {
    start_date = Time.zone.parse(start_date_str || '')
    return unless start_date
    where("appointment_slot > ?", start_date.beginning_of_day)
  }

  scope :filter_by_end_date_str, -> (end_date_str) {
    end_date = Time.zone.parse(end_date_str || '')
    return unless end_date
    where("appointment_slot < ?", end_date.end_of_day)
  }

  scope :filter_by_guide, -> (guide_id) {
    if !guide_id.blank? && guide_id != 0
      t = self.arel_table
      filter = t[:guide1_id].eq(guide_id).
        or(t[:guide2_id].eq(guide_id))
      where(filter)
    end
  }

  scope :filter_by_member, -> (member) {
    unless member.blank?
      t = Registration.arel_table
      filter = t[:member_email].matches("%#{member}%").
        or(t[:member_name].matches("%#{member}%"))
      where(filter)
    end
  }

  scope :unregistered, lambda {
    includes(:registration)
    .where(registrations: {id: nil})
  }

  scope :ordered, lambda { order(:appointment_slot) }

  scope :having_associations, lambda { joins(:location, :guide1) }

  scope :needs_reminder_email, -> (time, begin_time = nil) {
    end_time = time.end_of_day
    if begin_time
      begin_time = begin_time.beginning_of_day
    else
      begin_time = time.beginning_of_day
    end

    joins(:registration)
    .references(:registration)
    .where(appointment_slot: begin_time..end_time)
    .where(registrations: {reminder_email_sent: nil})
  }

  # TODO: These should probably be in a presenter.
  def guide1_name
    guide1 && guide1.name
  end

  def guide2_name
    guide2 && guide2.name || ''
  end

  def self.find_by_questionnaire_instance_id(instance_id)
    Registration.find_by(questionnaire_instance_id: instance_id).try(:appointment)
  end

  def self.get_dt_data(params, guide)
    total = Appointment.count
    columns = params['columns']

    order_map = params['order'].to_unsafe_h.map do |(_, order)|
      column = columns[order['column']]['name']
      dir = order['dir'].to_sym

      case column
      when 'appointment_type_id'
        "LOWER(appointment_types.name) #{dir}"
      when 'location_id'
        "LOWER(locations.name) #{dir}"
      when 'member'
        "LOWER(registrations.member_name) #{dir}, LOWER(registrations.member_email) #{dir}"
      when 'appointment_slot'
        "appointment_slot #{dir}"
      else
        "LOWER(#{column}) #{dir}"
      end
    end

    query = self.get_query_from_params(params)
      .order(order_map)

    filtered = query.dup.count

    query.limit!(params['length']).offset!(params['start'])

    appointments = query.map do |appointment|
      can_edit = guide.can_edit?(appointment)
      {
        DT_RowId: appointment.id,
        appointment_slot: appointment.appointment_slot,
        guide1_name: appointment.guide1_name,
        guide2_name: appointment.guide2_name,
        guide1_id: appointment.guide1_id,
        guide2_id: appointment.guide2_id,
        can_edit: can_edit,
        belongs_to_guide: guide.is_guide_for?(appointment),
        type_name: can_edit ? appointment.try(:appointment_type).try(:name) : nil,
        member_name: can_edit ? appointment.member_name : nil,
        member_email: can_edit ? appointment.member_email : nil,
        location_name: can_edit ? appointment.location.try(:name) : nil,
      }
    end

    {
      draw: params['draw'],
      recordsTotal: total,
      recordsFiltered: filtered,
      data: appointments
    }
  end

  def self.get_calendar_data(params, guide)
    query = self.get_query_from_params(params)
    query.where!('appointments.guide1_id = ? OR appointments.guide2_id = ?',
                 guide.id, guide.id) if params['self_only']
    query.map do |appointment|
      {
        id: appointment.id,
        title: "
          #{appointment.guide1_name}
          #{appointment.guide2_name}",
        start: appointment.appointment_slot,
        end: appointment.appointment_slot + MEETING_LENGTH,
        can_edit: guide.can_edit?(appointment),
        belongs_to_guide: guide.is_guide_for?(appointment),
        is_registered: !!appointment.registration
      }
    end
  end

  def assign_location
    return if location || !appointment_slot

    location_ids = Location.all.order(:id).pluck(:id)

    start_time = (appointment_slot - (MEETING_LENGTH - 1.minute))
    end_time = (appointment_slot + (MEETING_LENGTH - 1.minute))
    Appointment.where(appointment_slot: start_time..end_time).each do |nearby_appointment|
      location_ids.delete(nearby_appointment.location_id)
    end
    # Will be blank if no options are available, so validation will fail.
    self.location_id = location_ids.first
  end

  def guide_emails
    ccs = []
    ccs << guide1.display_email if guide1
    ccs << guide2.display_email if guide2
    ccs
  end

  def registration_display_email
    registration.try(:display_email)
  end

  def update_reminder_email_sent(time)
    registration.try(:update, {reminder_email_sent: time})
  end

  def update_questionnaire_done(time)
    registration.try(:update, {questionnaire_done: time})
  end

  def self.from_params(params)
    get_query_from_params(params).all
  end

  def guide_names
    names = [guide1_name]
    names << guide2.name if guide2
    names.join(' and ')
  end

  private
  def self.get_query_from_params(params)
    self.with_associations
      .references(:guide1, :guide2, :location)
      .filter_by_guide(params['guide'])
      .filter_by_member(params['member'])
      .filter_by_start_date_str(params['start_date'])
      .filter_by_end_date_str(params['end_date'])
  end
end
