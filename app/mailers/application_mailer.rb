class ApplicationMailer < ActionMailer::Base
  default from: Proc.new { "Highrock Guides Ministry <#{Setting.director_email}>" }
  layout 'mailer'
end
