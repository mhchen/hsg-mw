class GuidesMailer < ApplicationMailer
  helper ApplicationHelper

  class InvalidGuideError < StandardError; end

  def registration_email(appointment)
    @appointment = appointment
    @appointment_type = appointment.registration.appointment_type
    @location = appointment.location
    @location_name = @location.name
    unless @location.name.downcase.include? "'s office"
      @location_name = "the #{@location_name}"
    end
    @questionnaireDueDate = appointment.appointment_slot - 2.days
    @questionnaireUrl = "#{root_url}questionnaire/#{@appointment.questionnaire_instance_id}"

    mail(
      to: appointment.registration_display_email,
      subject: "Highrock Spiritual Guides Confirmation - Please Return Survey by #{@questionnaireDueDate.strftime('%A, %-m/%-d')} @ 5 PM",
      cc: @appointment.guide_emails,
      bcc: Setting.admin_email,
      template_name: @appointment_type.slug.underscore,
      template_path: 'guides_mailer/registration',
    )
  end

  def questionnaire_email(questionnaire)
    @questionnaire = questionnaire
    @appointment = questionnaire.appointment
    mail(
      to: @appointment.registration_display_email,
      subject: "#{@appointment.member_name} - Your Highrock Spiritual Guides #{@appointment.appointment_type.name.downcase} questionnaire",
      cc: @appointment.guide_emails
    )
  end

  def reminder_email(appointment)
    @appointment = appointment
    @appointment_type = appointment.appointment_type
    @location = appointment.location
    @location_name = @location.name
    unless @location.name.downcase.include? "'s office"
      @location_name = "the #{@location_name}"
    end
    @tomorrowDate = Time.now + 1.day
    mail(
      to: appointment.registration_display_email,
      subject: 'Highrock Spiritual Guides Appointment Reminder',
      cc: @appointment.guide_emails,
      bcc: Setting.admin_email
    )
  end

  def cancellation_email(appointment)
    @appointment = appointment
    mail(
      to: "#{appointment.member_name} <#{appointment.member_email}>",
      subject: 'Highrock Spiritual Guides Appointment Cancellation',
      cc: appointment.guide_emails,
      bcc: Setting.admin_email
    )
  end

  def welcome_email(guide)
    raise InvalidGuideError unless guide.password && guide.persisted?
    @root = ActionMailer::Base.default_url_options[:protocol] +
      ActionMailer::Base.default_url_options[:host]
    @guide = guide
    mail(
      to: guide.display_email,
      subject: 'Your new Highrock Spiritual Guides account'
    )
  end
end
