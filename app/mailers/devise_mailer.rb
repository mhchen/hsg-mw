class DeviseMailer < Devise::Mailer
  default from: Proc.new { "Highrock Guides Ministry <#{Setting.director_email}>" }
  default template_path: 'devise/mailer'

  def reset_password_instructions(record, token, opts={})
    @root = ActionMailer::Base.default_url_options[:protocol] +
      ActionMailer::Base.default_url_options[:host]
    super
  end
end
