var path = require('path');
var webpack = require('webpack');

var plugins = [];

var jsLoaders = ['ng-annotate', 'babel-loader?presets[]=es2015&cacheDirectory', 'imports?define=>false'];
if (['staging', 'production'].indexOf(process.env.RAILS_ENV) !== -1) {
  plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {warnings: false}
  }));
  plugins.push(new webpack.optimize.DedupePlugin());
}

var adminPlugins = plugins;

var appPlugins = plugins.concat([
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery',
    'window.$': 'jquery'
  })
]);


module.exports = {
  config: [{
    cache: true,
    devtool: 'source-maps',
    name: 'admin',
    entry: {
      admin: './src/javascripts/admin.js',
    },
    output: {
      path: path.resolve(__dirname, './public/assets/javascripts'),
      publicPath: '/assets/javascripts/',
      filename: '[name].js'
    },
    plugins: adminPlugins,
    module: {
      loaders: [{
        test: /\.js$/,
        include: [path.resolve(__dirname, './src/javascripts')],
        loaders: jsLoaders
      }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }, {
        test: /\.html$/,
        loader: 'ngtemplate?relativeTo=' + (path.resolve(__dirname)) + '/!html'
      }, {
        include: /\.json$/,
        loaders: ['json-loader']
      }, {
        test: /\.png$/,
        loader: 'url-loader',
        query: { mimetype: 'image/png' }
      }, {
        test: /\.gif$/,
        loader: 'url-loader',
        query: { mimetype: 'image/gif' }
      }, {
        test: /\.scss/, loader: 'style!css!sass?includePaths[]=' +
          (path.resolve(__dirname, './node_modules'))
      },
      {test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&minetype=application/font-woff' },
      {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader' }
      ]
    }
  }, {
    name: 'application',
    devtool: 'source-maps',
    entry: {
      application: './src/javascripts/application.js'
    },
    output: {
      path: path.resolve(__dirname, './public/assets/javascripts'),
      publicPath: '/assets/javascripts/',
      filename: '[name].js'
    },
    plugins: appPlugins,
    module: {
      loaders: [{
        test: /\.js$/,
        include: [path.resolve(__dirname, './src/javascripts')],
        loaders: ['babel-loader?presets[]=es2015&cacheDirectory']
      }]
    }
  }]
};
