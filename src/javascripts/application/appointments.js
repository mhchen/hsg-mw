const $modal = $('#js-modal-form');
const $modalDialog = $modal.find('.modal-dialog');
const $conflictError = $('#js-conflict-error')

$('.js-sign-up').click(function() {
  $conflictError.hide();
  var $this = $(this);
  $modalDialog.load(`/appointments/${$this.data('id')}/edit`, (data, statusText, response) => {
    if (response.status === 409) {
      $conflictError.show();
      $this.closest('tr').remove();
    } else {
      $modal.modal('show');
    }
  });
});

// If the modal is populated on creation, it's a filled-out form. Show it immediately.
if ($modalDialog.find('.modal-content').length) {
  $modal.modal('show');
}

// Focus first field on modal open.
$modal.on('shown.bs.modal', () => {
  $modal.find('input:not([type="hidden"])').first().focus();
});
