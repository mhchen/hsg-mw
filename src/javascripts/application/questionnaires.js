const $kidsAgesRow = $('#js-kids-ages-row');

$('input[name="questionnaire[kids]"]').change(function() {
  var val = $(this).val();
  $kidsAgesRow.toggle(val === 'Yes');
});
