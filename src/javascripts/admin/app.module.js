// TODO: Bug in ui-bootstrap, fixed when https://github.com/angular-ui/bootstrap/pull/5786
// gets merged.
import 'angular-ui-bootstrap/src/position';
import uiModal from 'angular-ui-bootstrap/src/modal';
import uiTabs from 'angular-ui-bootstrap/src/tabs';
import uiRouter from 'angular-ui-router';
import toastr from 'angular-toastr';
import uiSelect from 'ui-select';

import 'angular-toastr/dist/angular-toastr.css';
import 'ui-select/dist/select.css';

import AppointmentModalService from './appointments/AppointmentModalService';
import AppointmentResource from './appointments/appointment-resource';
import AppointmentTypeResource from './appointments/appointment-type-resource';
import AuthService from './auth/AuthService';
import GuideResource from './guides/guide-resource';
import LocationResource from './appointments/location-resource';
import appointmentCalendarDirective from './appointments/appointmentCalendarDirective';
import appointmentTableDirective from './appointments/appointmentTableDirective';
import dateParserDirective from './common/dateParserDirective';
import formGroupDirective from './formGroupDirective';
import navDirective from './navDirective';
import newPasswordFormDirective from './auth/newPasswordFormDirective';
import guideTableDirective from './guides/guideTableDirective';
import guideFilter from './guides/guide-filter';
import jqDatepickerDirective from './jq-datepicker-directive';
import jqTimepickerDirective from './jq-timepicker-directive';
import {routesRun, routesConfig} from './routes';
import stateFormDirective from './common/stateFormDirective';
import SettingsApi from './settings/SettingsApi';

angular.module('admin', [
  'ngAnimate',
  'ngMessages',
  uiTabs,
  uiModal,
  uiRouter,
  uiSelect,
  'ngResource',
  toastr
]);

var app = angular.module('admin');

app.config(routesConfig);
app.run(routesRun);

// Default updates to PUT method.
app.config(function($resourceProvider) {
  'ngInject';
  $resourceProvider.defaults.actions.update = {
    method: 'PUT'
  };
});

// Add global variables/utilities.
app.factory('moment', function() {
  return window.moment;
});

// Add ability for select fields to use numbers as ngModels.
// https://code.angularjs.org/1.4.7/docs/api/ng/directive/select
app.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return parseInt(val, 10);
      });
      ngModel.$formatters.push(function(val) {
        return '' + val;
      });
    }
  };
});

// Input match confirmation (for password fields).
app.directive('matchesText', () => {
  return {
    restrict: 'A',
    require: 'ngModel',
    scope: {
      matchesText: '='
    },
    link: (scope, element, attrs, ngModelCtrl) => {
      ngModelCtrl.$validators.matchesText = (value) => {
        return value === scope.matchesText;
      };

      scope.$watch('matchesText', () => {
        ngModelCtrl.$validate();
      });
    }
  }
});

// Register app-specific stuff.
app.directive('hsgAppointmentTable', appointmentTableDirective)
    .directive('hsgAppointmentCalendar', appointmentCalendarDirective)
    .directive('hsgDateParser', dateParserDirective)
    .directive('hsgNewPasswordForm', newPasswordFormDirective)
    .directive('hsgNav', navDirective)
    .directive('hsgFormGroup', formGroupDirective)
    .directive('hsgGuideTable', guideTableDirective)
    .directive('hsgJqDatepicker', jqDatepickerDirective)
    .directive('hsgJqTimepicker', jqTimepickerDirective)
    .directive('hsgStateForm', stateFormDirective)
    .factory('AppointmentResource',  AppointmentResource)
    .factory('AppointmentTypeResource', AppointmentTypeResource)
    .factory('AppointmentModalService', AppointmentModalService)
    .factory('AuthService', AuthService)
    .factory('GuideResource', GuideResource)
    .factory('LocationResource', LocationResource)
    .factory('SettingsApi', SettingsApi)
    .filter('hsgGuideFilter', guideFilter);
