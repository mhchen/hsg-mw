import templateUrl from './form-group.html';

// Bootstrap form group directive for error handling.
export default function() {
  'ngInject';

  return {
    restrict: 'E',
    require: '^^?form',
    templateUrl: templateUrl,
    transclude: {
      input: 'hsgFormGroupInput',
      errors: '?hsgFormGroupErrors'
    },
    replace: true,
    scope: {
      vertical: '@',
      inputName: '@',
      label: '@',
      errorMessages: '=',
      labelClass: '@'
    },
    link: function(scope, element, attrs, formCtrl) {
      const $form = element.closest('form');

      scope.isHorizontal = $form.hasClass('form-horizontal');
      scope.formCtrl = formCtrl;
      scope.isRequired = attrs.hasOwnProperty('required') ||
          element.find('hsg-form-group-input').children(':first').attr('required');
    }
  };
}
