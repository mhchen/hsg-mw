import AppointmentCtrl from './appointments/AppointmentCtrl';
import appointmentViewTemplate from './views/appointment.html';
import GuideCtrl from './guides/GuideCtrl';
import guideViewTemplate from './views/guide.html';
import AccountCtrl from './accounts/AccountCtrl';
import accountViewTemplate from './views/account.html';
import newPasswordViewTemplate from './views/new-password.html';
import NewPasswordCtrl from './auth/NewPasswordCtrl';
import resetPasswordViewTemplate from './views/reset-password.html';
import ResetPasswordCtrl from './auth/ResetPasswordCtrl';
import QuestionCtrl from './questions/question-ctrl';
import QuestionDetailCtrl from './questions/QuestionDetailCtrl';
import LocationsCtrl from './locations/LocationsCtrl';
import SettingsCtrl from './settings/SettingsCtrl';
import questionViewTemplate from './views/question.html';
import questionDetailViewTemplate from './views/question-detail.html';
import loginViewTemplate from './views/login.html';
import settingsViewTemplate from './views/settings.html';
import locationsViewTemplate from './views/locations.html';
import LoginCtrl from './auth/LoginCtrl';
import LogoutCtrl from './auth/LogoutCtrl';

export function routesRun($rootScope, AuthService, $state, $timeout) {
  'ngInject';

  $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromParams, error) => {
    if (error.status === 404) {
      $state.go('home');
    }
  });

  // Workaround to make sure we do a user check on every route.
  // See https://github.com/angular-ui/ui-router/issues/1399.
  $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
    const {authed, admin} = toState.data;

    if (toState.name === 'home') {
      fromParams.skipSomeAsync = false;
      return;
    }
    if (fromParams.skipSomeAsync) {
      return;
    }

    event.preventDefault();

    AuthService.getCurrentUser().then((user) => {
      fromParams.skipSomeAsync = true;
      if (authed) {
        if (!user) {
          return $state.go('login');
        }

        if (admin && !user.isAdmin()) {
          return $state.go('appointments');
        }
      }
      if (!authed && user) {
        return $state.go('appointments');
      }

      if (authed && user && user.mustChangePassword() && toState.name !== 'change-password') {
        return $state.go('change-password');
      }
      return $state.go(toState, toParams);
    });
  });
}

export function routesConfig($stateProvider, $locationProvider, $urlMatcherFactoryProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);

  // Allow trailing slashes.
  $urlMatcherFactoryProvider.strictMode(false);

  $stateProvider
      .state('home', {
        url: '/admin',
        data: {authed: false},
        onEnter: function($state) {
          $state.go('login');
        }
      })
      .state('login', {
        url: '/admin/login',
        templateUrl: loginViewTemplate,
        controllerAs: 'loginCtrl',
        controller: LoginCtrl,
        data: {authed: false},
      })
      .state('logout', {
        url: '/admin/logout',
        controller: LogoutCtrl,
        data: {authed: true},
      })
      .state('appointments', {
        url: '/admin/appointments',
        templateUrl: appointmentViewTemplate,
        controllerAs: 'appointmentCtrl',
        controller: AppointmentCtrl,
        data: {authed: true},
        resolve: {
          guides: function(GuideResource) {
            return GuideResource.query().$promise;
          }
        }
      })
      .state('guides', {
        url: '/admin/guides',
        controllerAs: 'guideCtrl',
        controller: GuideCtrl,
        templateUrl: guideViewTemplate,
        data: {
          authed: true,
          admin: true
        },
        resolve: {
          guides: function(GuideResource) {
            return GuideResource.query().$promise;
          }
        }
      })
      .state('settings', {
        url: '/admin/settings',
        controllerAs: 'settingsCtrl',
        controller: SettingsCtrl,
        templateUrl: settingsViewTemplate,
        data: {
          authed: true,
          admin: true
        },
        resolve: {
          settings: function(SettingsApi) {
            return SettingsApi.query();
          }
        }
      })
      .state('locations', {
        url: '/admin/locations',
        controllerAs: 'locationsCtrl',
        controller: LocationsCtrl,
        templateUrl: locationsViewTemplate,
        data: {
          authed: true,
          admin: true
        },
        resolve: {
          locations: function(LocationResource) {
            return LocationResource.query();
          }
        }
      })
      .state('account', {
        url: '/admin/account',
        controllerAs: 'accountCtrl',
        controller: AccountCtrl,
        templateUrl: accountViewTemplate,
        data: {authed: true},
      })
      .state('questionDetail', {
        url: '/admin/questions/:id',
        controllerAs: 'detailCtrl',
        controller: QuestionDetailCtrl,
        templateUrl: questionDetailViewTemplate,
        data: {
          authed: true,
          admin: true
        },
        resolve: {
          appointmentType: function(AppointmentTypeResource, $stateParams) {
            return AppointmentTypeResource.get({id: $stateParams.id}).$promise;
          }
        }
      })
      .state('reset-password', {
        url: '/admin/reset-password',
        params: {
          email: null
        },
        templateUrl: resetPasswordViewTemplate,
        controllerAs: 'resetCtrl',
        controller: ResetPasswordCtrl,
        data: {authed: false},
      })
      .state('change-password', {
        url: '/admin/change-password',
        templateUrl: newPasswordViewTemplate,
        controllerAs: 'newCtrl',
        controller: NewPasswordCtrl,
        data: {authed: true}
      })
      .state('new-password', {
        url: '/admin/new-password/:token',
        templateUrl: newPasswordViewTemplate,
        controllerAs: 'newCtrl',
        controller: NewPasswordCtrl,
        data: {authed: false},
        resolve: {
          guide: function($stateParams, $http) {
            return $http.get('/admin/api/guides/from-token', {
              params: {
                token: $stateParams.token
              }
            }).then((response) => {
              return response.data;
            });
          }
        }
      })
      .state('questions', {
        url: '/admin/questions',
        templateUrl: questionViewTemplate,
        controllerAs: 'questionCtrl',
        controller: QuestionCtrl,
        data: {
          authed: true,
          admin: true
        },
        resolve: {
          appointmentTypes: function(AppointmentTypeResource) {
            return AppointmentTypeResource.query().$promise;
          }
        }
      })
      .state('otherwise', {
        url: '*path',
        data: {authed: false},
        onEnter: function($state) {
          $state.go('login');
        }
      })
}
