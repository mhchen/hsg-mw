export default function(AuthService, $scope, toastr, $http) {
  'ngInject';

  AuthService.getCurrentUser().then((user) => {
    this.guide = angular.copy(user.userData);
  });

  this.submit = () => {
    if ($scope.infoForm.$invalid) {
      return;
    }

    const promise = $http.post('/admin/api/guides/me', this.guide);
    $scope.updateFormState(promise);
    promise.catch((response) => {
      const emailError = response.data.email && response.data.email[0];
      if (emailError && emailError.error === 'taken') {
        $scope.infoForm.email.$setValidity('taken', false);
      } else {
        toastr.error('Please try again later', 'Something went wrong');
      }
    });
  };
};
