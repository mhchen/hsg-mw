export default function($resource) {
  'ngInject';

  return $resource('/admin/api/guides/:id', {id: '@id'});
}
