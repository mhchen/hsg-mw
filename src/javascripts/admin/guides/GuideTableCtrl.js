import GuideEditModalCtrl from './GuideEditModalCtrl';
import editModalTemplateUrl from './guide-edit-modal.html';
import GuideDeleteModalCtrl from './GuideDeleteModalCtrl';
import deleteModalTemplateUrl from './guide-delete-modal.html';

export default function($scope, $uibModal, GuideResource, $http) {
  'ngInject';

  this.$scope = $scope;
  this.openEditModal = (guide) => {
    $uibModal.open({
      templateUrl: editModalTemplateUrl,
      controllerAs: 'modalCtrl',
      controller: GuideEditModalCtrl,
      resolve: {
        title: () => `Edit guide: ${guide.first_name} ${guide.last_name}`,
        submitText: () => 'Save',
        guide: () => guide,
      }
    }).result.then(function(guide) {
      $scope.$emit('guideSave', guide);
    });
  };

  this.openDeleteModal = guide => {
    $uibModal.open({
      templateUrl: deleteModalTemplateUrl,
      controllerAs: 'modalCtrl',
      controller: GuideDeleteModalCtrl,
      resolve: {
        guide: () => guide,
        appointmentCount: () => {
          return $http.get('/admin/api/guides/count-dependent-appointments', {
            params: {id: guide.id}
          }).then((response) => {
            return parseInt(response.data, 10);
          });
        }
      }
    }).result.then(guide => {
      $scope.$emit('guideDelete', guide);
    });
  };
}
