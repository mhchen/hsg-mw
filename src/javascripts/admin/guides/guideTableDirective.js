import HsgGuideTableCtrl from './GuideTableCtrl';

export default function(GuideResource, $compile) {
  'ngInject';

  return {
    restrict: 'A',
    controllerAs: 'tableCtrl',
    controller: HsgGuideTableCtrl,
    scope: {
      guides: '='
    },
    link: function(scope, element) {
      // Store an ID hash of all the rows.
      var rows = {};

      var appendActions = (row, data) => {
        var $row = angular.element(row);

        rows[data.id] = $row;

        var newScope = scope.$new();
        newScope.guide = data;
        var actionsElement = $compile(
          `<button class="btn btn-primary" ng-click="tableCtrl.openEditModal(guide)">Edit</button>
          <button class="btn btn-danger" ng-click="tableCtrl.openDeleteModal(guide)">Delete</button>`
        )(newScope);
        $row.find('.actions').append(actionsElement);
      };

      const dt = element.DataTable({
        data: scope.guides,
        createdRow: appendActions,
        columns: [{
          data: 'first_name',
          title: 'First name'
        }, {
          data: 'last_name',
          title: 'Last name'
        }, {
          data: 'email',
          title: 'Email'
        }, {
          data: 'is_admin',
          title: 'Is admin?',
          render: data => data ? 'Yes' : 'No'
        }, {
          className: 'actions',
          searchable: false,
          sortable: false,
          data: function() {
            // Return a blank column that will be populated by createRow later.
            return '';
          }
        }]
      });

      scope.$on('guideSave', function(event, guide) {
        var $row = rows[guide.id];
        if ($row) {
          dt.row($row).data(guide).draw(false);
          appendActions($row, guide);
        } else {
          dt.row.add(guide).draw(false);
        }
      });

      scope.$on('guideDelete', function(event, guide) {
        var $row = rows[guide.id];
        if ($row) {
          dt.row($row).remove().draw(false);
        }
      });
    }
  };
}
