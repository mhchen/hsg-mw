export default function($uibModalInstance, guide, title, submitText, toastr, $scope) {
  'ngInject';

  this.title = title;
  this.submitText = submitText;
  this.guide = angular.copy(guide);

  this.close = () => $uibModalInstance.dismiss();

  var onFailure = (response) => {
    var emailError = response.data.email && response.data.email[0];
    if (emailError && emailError.error === 'taken') {
      $scope.editForm.email.$setValidity('taken', false);
    } else {
      $uibModalInstance.dismiss();
      toastr.error('Please try again later', 'Something went wrong');
    }
  };

  // Always clear email validity when edited.
  $scope.$watch(() => {
    return this.guide.email;
  }, () => {
    $scope.editForm.email.$setValidity('taken', true);
  });

  this.submit = () => {
    if ($scope.editForm.$invalid) {
      return;
    }
    let promise;
    if (this.guide.id) {
      promise = this.guide.$update().then(() => {
        $uibModalInstance.close(this.guide);
        toastr.success('Guide successfully updated!');
      });
    } else {
      if (this.noEmail) {
        this.guide.no_email = true;
      }
      promise = this.guide.$save().then(() => {
        $uibModalInstance.close(this.guide);
        toastr.success('Guide successfully created!');
      });
    }
    $scope.updateFormState(promise);
    return promise.catch(onFailure);
  };
}
