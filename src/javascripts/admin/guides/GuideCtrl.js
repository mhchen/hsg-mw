import GuideEditModalCtrl from './GuideEditModalCtrl';
import editModalTemplateUrl from './guide-edit-modal.html';
import GuideDeleteModalCtrl from './GuideDeleteModalCtrl';
import deleteModalTemplateUrl from './guide-delete-modal.html';

export default function($scope, $uibModal, guides, GuideResource) {
  'ngInject';

  this.guides = guides;

  this.$scope = $scope;
  this.openNewModal = () => {
    $uibModal.open({
      templateUrl: editModalTemplateUrl,
      controllerAs: 'modalCtrl',
      controller: GuideEditModalCtrl,
      resolve: {
        title: () => 'New guide',
        submitText: () => 'Save',
        guide: () => new GuideResource(),
      }
    }).result.then(guide => {
      $scope.$broadcast('guideSave', guide);
    });
  };
}
