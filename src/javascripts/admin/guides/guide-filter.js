export default function() {
  'ngInject';

  return function(guides, search) {
    var lowercaseSearch = search.toLowerCase();
    if (guides) {
      return guides.filter(function(guide) {
        var lowercaseFullName = `${guide.first_name} ${guide.last_name}`.toLowerCase();
        return lowercaseFullName.includes(lowercaseSearch);
      });
    }
    return [];
  };
}
