export default function(guide, $uibModalInstance, toastr, appointmentCount) {
  'ngInject';
  this.appointmentCount = appointmentCount;
  this.guide = guide;

  this.close = () => $uibModalInstance.dismiss();

  this.submit = () => {
    this.isProcessing = true;
    guide.$delete().then(() => {
      $uibModalInstance.close(guide);
      toastr.success('Guide successfully deleted!');
    }).catch(() => {
      $uibModalInstance.dismiss();
      toastr.error('Please try again later', 'Something went wrong');
    }).finally(() => {
      this.isProcessing = false;
    });
  };
}
