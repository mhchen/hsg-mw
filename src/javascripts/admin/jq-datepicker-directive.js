import 'bootstrap-datepicker/dist/js/bootstrap-datepicker';

export default function() {
  return {
    require: ['ngModel', '^^?form'],
    link: function(scope, element, attrs, controllers) {
      var ngModelCtrl, formCtrl;
      [ngModelCtrl, formCtrl] = controllers;
      // Wait until the model value has initialized.
      var unregister = scope.$watch(function() {
        return ngModelCtrl.$viewValue;
      }, function() {
        var name = element.attr('name');
        var isPristine;
        if (formCtrl) {
          isPristine = formCtrl[name].$pristine;
        }
        element.datepicker({
          autoclose: true,
          todayHighlight: true
        });

        if (formCtrl && isPristine) {
          formCtrl[name].$setPristine();
        }
        unregister();
      });
    }
  };
}
