export default function(appointmentCount, locationCount, $uibModalInstance, toastr) {
  'ngInject';

  this.appointmentCount = appointmentCount;
  this.locationCount = locationCount;

  this.close = () => $uibModalInstance.dismiss('modalDismissed');
  this.submit = () => $uibModalInstance.close();
}
