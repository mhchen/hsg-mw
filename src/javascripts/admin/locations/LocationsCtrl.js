import deleteModalTemplateUrl from './location-delete-modal.html';
import LocationDeleteModalCtrl from './LocationDeleteModalCtrl';

export default function(locations, LocationResource, $uibModal, $scope, toastr, $q, $http) {
  'ngInject';

  this.locations = locations;

  this.new = () => {
    this.locations.push({
      name: '',
      description: ''
    });
  };

  this.delete = (location) => {
    const index = this.locations.indexOf(location);
    if (index > -1) {
      if (location.id) {
        location._delete = true;
      } else {
        this.locations.splice(index, 1);
      }
    }
  };

  this.submit = () => {
    if (!$scope.form.$valid) {
      return;
    }

    let promise;

    const locationsToDelete = [];
    this.locations.forEach((location) => {
      if (location._delete) {
        locationsToDelete.push(location.id);
      }
    });

    if (locationsToDelete.length) {
      // Open a confirmation modal to indicate the implications of deleting locations.
      promise =  $http.get('/admin/api/locations/count-dependent-appointments', {
        params: {
          'location_ids[]': locationsToDelete
        }
      }).then((response) => {
        const appointmentCount = parseInt(response.data, 10);
        if (appointmentCount) {
          return $uibModal.open({
            templateUrl: deleteModalTemplateUrl,
            controllerAs: 'modalCtrl',
            controller: LocationDeleteModalCtrl,
            resolve: {
              appointmentCount: appointmentCount,
              locationCount: locationsToDelete.length
            }
          }).result;
        }
      });
    } else {
      promise = $q.resolve();
    }

    $scope.updateFormState(promise);

    promise.then(() => {
      return LocationResource.save({locations: this.locations}).$promise.catch((e) => {
        toastr.error('Please try again later', 'Something went wrong');
        throw e;
      });
    });
  };
}
