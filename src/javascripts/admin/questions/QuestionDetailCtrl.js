export default function(appointmentType, toastr, $scope) {
  'ngInject';

  this.appointmentType = appointmentType;

  this.addQuestionToCategory = (category) => {
    category.questions.push({});
  };

  this.deleteQuestion = (question) => {
    question._destroy = true;
  };

  this.submit = () => {
    const promise = appointmentType.$update()
    $scope.updateFormState(promise);

    promise.catch(() => {
      toastr.error('Please try again later', 'Something went wrong');
    })
  };
}
