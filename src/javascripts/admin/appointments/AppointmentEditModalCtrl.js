export default function($uibModalInstance, guides, locations, appointmentTypes, title, submitText, appointment, moment, toastr, $scope, AuthService, appointmentProps, user) {
  'ngInject';

  this.title = title;
  this.submitText = submitText;
  this.guides = guides;
  this.appointment = angular.extend(appointment, (appointmentProps || {}));
  this.appointmentTypes = appointmentTypes;
  this.locations = locations;

  // Set defaults for select ngModels.
  this.appointment.location_id = this.appointment.location_id || '';
  this.appointment.appointment_type_id = this.appointment.appointment_type_id || '';
  this.appointment.guide1_id = this.appointment.guide1_id || '';
  this.appointment.guide2_id = this.appointment.guide2_id || '';

  if (appointment.id) {
    const appointmentSlot = moment(this.appointment.appointment_slot).tz('America/New_York');
    this.appointment.appointment_slot_date = appointmentSlot.format('MM/DD/YYYY');
    this.appointment.appointment_slot_time = appointmentSlot.format('h:mma');
  }

  // Lock the guide input that refers to the current user.
  if (!user.isAdmin()) {
    if (appointment.id) {
      this.isGuide1Disabled = appointment.guide1_id === user.getId();
      this.isGuide2Disabled = appointment.guide2_id === user.getId();
    } else {
      // Select and lock the first guide user.
      this.isGuide1Disabled = true;
      this.appointment.guide1_id = user.getId();
    }
  }

  this.close = () => $uibModalInstance.dismiss();

  this.isRegistrationActive = () => {
    return this.appointment.registration && !this.appointment.registration._destroy;
  };

  this.isRegistrationNew = () => {
    return !this.appointment.id || !this.appointment.registration || !this.appointment.registration.id;
  }

  this.showRegistration = () => {
    if (this.appointment.registration) {
      this.appointment.registration._destroy = false;
    } else {
      this.appointment.registration = {
        appointment_type_id: ''
      };
      this.appointment.send_confirmation_email = true;
    }
  };

  this.deleteRegistration = () => {
    if (this.appointment.registration.id) {
      this.appointment.registration._destroy = true;
    } else {
      this.appointment.registration = null;
    }
  };

  const onFailure = (response) => {
    // If there is a location conflict, it's a date/time issue.
    if (response.data.location) {
      this.globalError = 'There aren\'t any rooms available for this date and time. Please schedule a different time.';
      $scope.editForm.date.$setValidity('locationConflict', false);
      $scope.editForm.time.$setValidity('locationConflict', false);
    } else if (response.data.error) {
      this.globalError = response.data.error;
    }
  };

  // Always clear the location conflict when date or time are edited.
  $scope.$watch(() => {
    return this.appointment.appointment_slot_date + '|' + this.appointment.appointment_slot_time;
  }, () => {
    if ($scope.editForm.date) {
      $scope.editForm.date.$setValidity('locationConflict', true);
    }
    if ($scope.editForm.time) {
      $scope.editForm.time.$setValidity('locationConflict', true);
    }
  });


  this.submit = ($event) => {
    if ($scope.editForm.$invalid) {
      $event.preventDefault();
      return;
    }
    $scope.$emit('startProcessing');

    let promise;

    // Make association Rails-friendly.
    this.appointment.registration_attributes = this.appointment.registration;

    if (this.appointment.id) {
      promise = this.appointment.$update().then(() => {
        $uibModalInstance.close();
        toastr.success('Appointment successfully updated!');
      });
    } else {
      promise = this.appointment.$save().then(() => {
        $uibModalInstance.close();
        toastr.success('Appointment successfully created!');
      });
    }
    $scope.updateFormState(promise);
    promise.catch(onFailure);
  };
}
