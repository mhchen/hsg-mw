export default function($resource) {
  'ngInject';

  return $resource('/admin/api/appointments/:id', {id: '@id'});
}
