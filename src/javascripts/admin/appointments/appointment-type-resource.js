export default function($resource) {
  'ngInject';

  return $resource('/admin/api/appointment_types/:id', {id: '@id'});
}
