import AppointmentTableCtrl from './AppointmentTableCtrl';

const AJAX_URL = '/admin/api/appointments';

export default function(moment, $compile, AuthService) {
  'ngInject';

  const styleDisallowedCell = (td, data, appointment) => {
    if (!appointment.can_edit) {
      angular.element(td).addClass('td-disallowed');
    }
  };

  return {
    require: ['^^ngController', 'hsgAppointmentTable'],
    controllerAs: 'tableCtrl',
    controller: AppointmentTableCtrl,
    scope: {},
    link(scope, element, attrs, ctrls) {
      const [appointmentCtrl, tableCtrl] = ctrls;

      let columns = [{
        data: 'appointment_slot',
        title: 'Time slot',
        name: 'appointment_slot',
        render: (data) => {
          // Always render in Eastern Time.
          const date = moment(data).tz('America/New_York');
          return `${date.format('MM/DD/YYYY')}<br>
          ${date.format('hh:mm a z')}`;
        }
      }, {
        title: 'Guides',
        data: 'guide1_name',
        name: 'guide1_id',
        sortable: false,
        render: (data, type, row) => {
          return `${row.guide1_name || '<span class="text-incomplete">Unassigned</span>'}<br>
          ${row.guide2_name || '<span class="text-incomplete">Unassigned</span>'}`;
        }
      }, {
        data: 'location_name',
        title: 'Location',
        name: 'location_id',
        className: 'dt-location',
        createdCell: styleDisallowedCell,
      }, {
        data: 'member_name',
        title: 'Member',
        render: (data, type, row) => {
          if (row.member_email) {
            return `${row.member_name}<br>
            ${row.member_email}`;
          } else {
            return '';
          }
        },
        createdCell: styleDisallowedCell,
        name: 'member'
      }, {
        title: 'Appointment type',
        data: 'type_name',
        name: 'appointment_type_id',
        createdCell: styleDisallowedCell,
      }, {
        className: 'js-actions dt-actions',
        searchable: false,
        sortable: false,
        data: () => {
          // Return a blank column that will be populated by createRow later.
          return '';
        },
        createdCell: styleDisallowedCell,
      }];

      const dt = element.DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        footer: true,
        createdRow: (row, appointment) => {
          if (!appointment.can_edit) {
            return;
          }
          const actionsHtml = `<button class="btn btn-primary" ng-click="tableCtrl.openEditModal(appointment)">Edit</button>
              <button class="btn btn-danger" ng-click="tableCtrl.openCancelModal(appointment)">Delete</button>`;

          const $row = angular.element(row);
          if (appointment.belongs_to_guide) {
            $row.addClass('tr-own');
          }

          const newScope = scope.$new();
          newScope.appointment = appointment;
          const actionsElement = $compile(actionsHtml)(newScope);
          $row.find('.js-actions').append(actionsElement);
        },
        ajax: {
          url: AJAX_URL,
          data: (data) => {
            // Convert the filters form to a key-value object.
            return angular.extend(data, appointmentCtrl.getFilterValues());
          }
        },
        columns: columns
      });

      appointmentCtrl.$scope.$on('filterChange', function() {
        dt.ajax.reload();
      });

      const reloadTableWithoutReset = dt.ajax.reload.bind(dt.ajax, null, false);

      appointmentCtrl.$scope.$on('listUpdate', reloadTableWithoutReset);
      scope.$on('listUpdate', reloadTableWithoutReset);
    }
  };
}
