export default function($resource) {
  'ngInject';

  return $resource('/admin/api/locations');
};
