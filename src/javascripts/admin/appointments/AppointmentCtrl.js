export default function($scope, AppointmentModalService, $httpParamSerializer, guides, AppointmentResource, AppointmentTypeResource, LocationResource, AuthService, moment) {
  'ngInject';

  this.$scope = $scope;
  AuthService.getCurrentUser().then((user) => {
    this.user = user;
  });

  $scope.$watch(() => {
    return this.activeTab;
  }, () => {
    $scope.$emit('activeTabChanged', this.activeTab);
  });

  // Set initial filter values.
  this.startDateFilter = moment();
  this.guides = guides;
  this.getFilterValues = () => {
    return {
      start_date: this.startDateFilter && this.startDateFilter.format('YYYY-MM-DD'),
      end_date: this.endDateFilter && this.endDateFilter.format('YYYY-MM-DD'),
      guide: this.guideFilter,
      member: this.memberFilter
    };
  };

  this.filterChange = () => {
    $scope.$emit('filterChange');
  };

  this.openNewModal = () => {
    AppointmentModalService.openEditModal()
        .result.then(() => {
          $scope.$emit('listUpdate');
        });
  };

  this.getCsvDownloadUrl = () => {
    const queryString = $httpParamSerializer(this.getFilterValues());
    return '/admin/appointments/csv?' + queryString;
  };
}
