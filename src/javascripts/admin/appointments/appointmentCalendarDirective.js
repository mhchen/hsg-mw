import AppointmentTableCtrl from './AppointmentTableCtrl';
import moment from 'moment';

const AJAX_URL = '/admin/api/appointments';

export default function($compile, AuthService, $timeout, AppointmentModalService) {
  'ngInject';

  const meetingLength = 90;

  return {
    require: ['^^ngController'],
    scope: {},
    link(scope, element, attrs, ctrls) {
      let rendered = false;
      const [appointmentCtrl] = ctrls;

      const refetchEvents = () => {
        if (rendered) {
          element.fullCalendar('refetchEvents');
        }
      }

      scope.$watch(() => {
        return appointmentCtrl.showSelfOnly;
      }, refetchEvents);
      appointmentCtrl.$scope.$on('listUpdate', refetchEvents);

      element.fullCalendar({
        startParam: 'start_date',
        endParam: 'end_date',
        eventClick: (event) => {
          if (event.belongs_to_guide) {
            AppointmentModalService.openEditModal(event.id).result.then(() => {
              appointmentCtrl.$scope.$emit('listUpdate');
            });
          }
        },
        dayClick: (date) => {
          AppointmentModalService.openEditModal(null, {
            appointment_slot_date: date.format('L')
          }).result.then(() => {
            appointmentCtrl.$scope.$emit('listUpdate');
          });
        },
        eventAfterAllRender: () => {
          rendered = true;
        },
        events: {
          url: '/admin/api/appointments/calendar',
          data: () => {
            const data = {};
            if (appointmentCtrl.showSelfOnly) {
              data.self_only = true;
            }
            return data;
          },
          eventDataTransform: (appointment) => {
            if (appointment.belongs_to_guide) {
              if (appointment.is_registered) {
                appointment.className = 'fc-event-mine-populated';
              } else {
                appointment.className = 'fc-event-mine'
              }
            }
            return appointment;
          }
        }
      });

      appointmentCtrl.$scope.$on('activeTabChanged', (e, activeTab) => {
        // Re-render when switching to calendar tab.
        // Gross arbitrary timeout but this seems inconsistent.
        if (activeTab === 1) {
          $timeout(() => {
            element.fullCalendar('render');
          }, 100);
        }
      });

    }
  };
}
