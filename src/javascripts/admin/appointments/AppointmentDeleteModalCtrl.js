export default function($uibModalInstance, appointment, toastr, moment) {
  'ngInject';

  this.appointment = appointment;

  var appointmentSlot = moment(this.appointment.appointment_slot).tz('America/New_York');
  this.timeSlotText = appointmentSlot.format('M/D/YYYY h:mma');

  this.close = () => $uibModalInstance.dismiss();

  this.submit = () => {
    this.isProcessing = true;
    appointment.$delete({no_send_email: this.noSendEmail}).then(() => {
      $uibModalInstance.close();
      toastr.success('Appointment successfully deleted!');
    }).catch(() => {
      $uibModalInstance.dismiss();
      toastr.error('Please try again later', 'Something went wrong');
    }).finally(() => {
      this.isProcessing = false;
    });
  };
}
