import deleteModalTemplateUrl from './appointment-delete-modal.html';
import editModalTemplateUrl from './appointment-edit-modal.html';
import AppointmentDeleteModalCtrl from './AppointmentDeleteModalCtrl';
import AppointmentEditModalCtrl from './AppointmentEditModalCtrl';

export default function($uibModal, GuideResource, AppointmentResource, AppointmentTypeResource, LocationResource, AuthService, $q) {
  'ngInject';

  let isModalActive = false;
  return {
    openEditModal: (appointmentId, appointmentProps) => {
      if (isModalActive) {
        return {
          result: $q.reject()
        }
      }

      isModalActive = true;
      let instance = $uibModal.open({
        templateUrl: editModalTemplateUrl,
        controllerAs: 'modalCtrl',
        controller: AppointmentEditModalCtrl,
        resolve: {
          title: () => appointmentId ? 'Edit appointment' : 'New appointment',
          submitText: () => appointmentId ? 'Update' : 'Save',
          appointment: () => {
            if (appointmentId) {
              return AppointmentResource.get({id: appointmentId}).$promise;
            }
            return new AppointmentResource();
          },
          user: () => AuthService.getCurrentUser(),
          appointmentProps: () => appointmentProps,
          guides: GuideResource.query().$promise,
          locations: LocationResource.query().$promise,
          appointmentTypes: AppointmentTypeResource.query().$promise
        }
      });

      instance.result.finally(() => {
        isModalActive = false;
      });

      return instance;
    },
    openCancelModal: (appointmentId) => {
      if (isModalActive) {
        return {
          result: $q.reject()
        }
      }

      isModalActive = true;

      const instance = $uibModal.open({
        templateUrl: deleteModalTemplateUrl,
        controllerAs: 'modalCtrl',
        controller: AppointmentDeleteModalCtrl,
        resolve: {
          appointment: AppointmentResource.get({id: appointmentId}).$promise
        }
      });

      instance.result.finally(() => {
        isModalActive = false;
      });

      return instance;
    }
  };
};
