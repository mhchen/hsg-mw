
export default function($uibModal, AppointmentModalService, AppointmentResource, AppointmentTypeResource, GuideResource, LocationResource, $scope) {
  'ngInject';

  this.openEditModal = (appointment) => {
    AppointmentModalService.openEditModal(appointment.DT_RowId).result.then(() => {
      $scope.$emit('listUpdate');
    });
  };

  this.openCancelModal = (appointment) => {
    AppointmentModalService.openCancelModal(appointment.DT_RowId).result.then(() => {
      $scope.$emit('listUpdate');
    });
  };
}
