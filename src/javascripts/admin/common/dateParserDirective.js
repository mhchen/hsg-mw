export default function(moment) {
  'ngInject';

  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      ngModelCtrl.$parsers.push(value => {
        if (!value) {
          return null;
        }
        const parsed = moment(value, 'L');
        if (parsed.isValid() && parsed.format('L') === value) {
          return parsed;
        }
      })
      ngModelCtrl.$formatters.push(value => {
        return value && value.format('L');
      })
    }
  };
}
