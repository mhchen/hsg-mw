import stateFormView from './state-form.html';
import StateFormCtrl from './StateFormCtrl';

export default function() {
  'ngInject';

  return {
    retrict: 'A',
    templateUrl: stateFormView,
    transclude: true,
    controllerAs: 'hsgStateFormCtrl',
    controller: StateFormCtrl,
    link: (scope, element, attrs, ctrl) => {
      ctrl.noSuccessMessage = attrs.hasOwnProperty('noSuccessMessage');
    }
  }
}
