export default function($scope, $timeout) {
  'ngInject';

  const successMessageTimeout = 5000;

  const stopProcessing = () => {
    this.isProcessing = false;
  };

  $scope.updateFormState = (promise, successMessage = 'Changes successfully saved!') => {
    this.isProcessing = true;
    if (!this.noSuccessMessage) {
      promise = promise.then(() => {
        this.successMessage = successMessage;
      })
    }
    return promise.finally(stopProcessing);
  }


  $scope.$on('stopProcessing', () => {
    this.isProcessing = false;
  });

  $scope.$on('processingSuccess', (e, ) => {
    this.successMessage = successMessage;
  });
}
