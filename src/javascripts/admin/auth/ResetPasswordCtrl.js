export default function($scope, $state, $stateParams, AuthService, toastr) {
  'ngInject';

  this.email = $stateParams.email;

  this.submit = () => {
    const email = this.email;

    if ($scope.form.$invalid) {
      return;
    }
    this.globalError = '';
    const promise = AuthService.resetPassword(this.email).then(() => {
      this.email = email;
    });
    $scope.updateFormState(promise, `An email has been sent to ${this.email} with instructions on how to reset your password.`);
    promise.catch((response) => {
      if (response.status === 404) {
        this.globalError = 'Sorry, there doesn\'t seem to be a guide with that email address';
      }
    });
  };
};
