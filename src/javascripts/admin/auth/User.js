export default class User {
  constructor(userData) {
    this.userData = userData;
  }
  getId() {
    return this.userData.id;
  }
  getFullName() {
    return `${this.userData.first_name} ${this.userData.last_name}`;
  }
  isAdmin() {
    return this.userData.is_admin;
  }
  mustChangePassword() {
    return this.userData.must_change_password;
  }
}
