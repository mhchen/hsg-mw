import NewPasswordFormCtrl from './NewPasswordFormCtrl';
import newPasswordFormViewTemplate from '../views/new-password-form.html';

export default function() {
  return {
    controllerAs: 'formCtrl',
    controller: NewPasswordFormCtrl,
    templateUrl: newPasswordFormViewTemplate,
    scope: {
      token: '@'
    },
    bindToController: true
  }
};
