export default function($http, $state, toastr, $timeout, $document, $scope) {
  'ngInject';

  const standaloneForm = $state.current.name === 'new-password';

  this.minLength = 6;

  this.submit = () => {
    if ($scope.passwordForm.$invalid) {
      return;
    }
    const data = {
      password: this.password,
      password_confirm: this.passwordConfirm,
    };

    if (standaloneForm) {
      data.token = this.token;
    }
    const promise = $http.post('/admin/api/guides/new-password', data);
    let successMessage = `Password successfully updated!`
    if (standaloneForm) {
      successMessage += ' Logging you in now\u2026';
    }
    $scope.updateFormState(promise, successMessage);
    promise.then(() => {
      if (standaloneForm) {
        $timeout(() => {
          $state.go('appointments');
        }, 2000);
      }
    });
  }
};
