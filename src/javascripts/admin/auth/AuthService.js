import User from './User';

export default function($http, $q, $rootScope, toastr) {
  'ngInject';

  let authPromise = null;

  $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
    if (!fromParams.user) {
      authPromise = null;
    }
  });

  let currentUser = null;

  const handleUserResponse = (response) => {
    const lastUser = currentUser;
    currentUser = null;
    if (response.data) {
      currentUser = new User(response.data);
    }
    if (currentUser !== lastUser) {
      emitUserChanged();
    }
    return currentUser;
  };

  const emitUserChanged = () => {
    $rootScope.$emit('userChanged', currentUser);
  };

  return {
    UNAUTHORIZED: 0,
    OK: 1,
    // Returns whether the user is authenticated.
    getCurrentUser() {
      authPromise = authPromise || $http.get('/admin/api/guides/sessions/current-user');
      return authPromise.then(handleUserResponse);
    },
    // Opposite of ensureAuthed.
    ensureNotAuthed() {
      return this.getCurrentUser().then((user) => {
        if (user) {
          throw 'User logged in';
        } else {
          return true;
        }
      });
    },
    login(params) {
      return $http.post('/admin/api/guides/sessions/sign_in', params)
          .then(handleUserResponse);
    },
    logout() {
      return $http.delete('/admin/api/guides/sessions/sign_out').then(() => {
        currentUser = null;
        emitUserChanged();
      });
    },
    resetPassword(email) {
      return $http.post('/admin/api/guides/reset-password', {email});
    }
  };
}
