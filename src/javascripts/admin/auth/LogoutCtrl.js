export default function($state, AuthService) {
  'ngInject';

  AuthService.logout().then(function() {
    $state.go('login');
  });
}
