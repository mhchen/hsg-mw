export default function($scope, $state, AuthService, toastr) {
  'ngInject';

  this.login = () => {
    if ($scope.form.$invalid) {
      return;
    }

    this.globalError = null;

    const promise = AuthService.login({
      password: this.password,
      email: this.email
    });
    $scope.updateFormState(promise);

    return promise.then(() => {
      $state.go('appointments');
    }).catch((response) => {
      if (response.status === 401) {
        this.globalError = response.data.error;
      } else {
        toastr.error('Please try again later', 'Something went wrong');
      }
    });
  };
}
