import navTemplate from './views/nav.html';

export default function() {
  'ngInject';

  return {
    templateUrl: navTemplate,
    scope: {},
    controllerAs: 'navCtrl',
    controller: function($rootScope, AuthService) {
      'ngInject';

      $rootScope.$on('userChanged', (e, user) => {
        this.user = user;
      });
    }
  }
}
