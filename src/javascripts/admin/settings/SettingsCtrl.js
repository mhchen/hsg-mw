export default function(settings, SettingsApi, $scope, toastr) {
  'ngInject';

  this.settings = settings;

  this.submit = () => {
    if (!$scope.form.$valid) {
      return;
    }

    $scope.$emit('startProcessing');

    const promise = SettingsApi.save(this.settings);
    $scope.updateFormState(promise);
    promise.catch(() => {
      toastr.error('Please try again later', 'Something went wrong');
    });
  }
}
