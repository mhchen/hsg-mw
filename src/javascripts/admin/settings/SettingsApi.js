export default ($http) => {
  'ngInject';
  const endpoint = '/admin/api/settings';

  return {
    query: () => {
      return $http.get(endpoint).then((response) => {
        return response.data;
      });
    },
    save: (settings) => {
      return $http.post(endpoint, {settings});
    }
  }
};
