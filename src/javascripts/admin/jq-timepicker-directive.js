export default function() {
  return {
    link: function(scope, element) {
      element.timepicker({
        step: 15,
        scrollDefault: '12:00pm'
      });
    }
  };
}
