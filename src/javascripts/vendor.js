// jQuery must go before angular.
require('jquery');

require('angular');
require('angular-animate');
require('angular-messages');
require('angular-resource');
require('angular-toastr');
require('angular-ui-bootstrap');
require('angular-ui-router');
require('bootstrap');
require('bootstrap-datepicker');
require('datatables.net');
require('datatables.net-bs');
require('moment');
require('fullcalendar');
require('timepicker');
require('ui-select');
