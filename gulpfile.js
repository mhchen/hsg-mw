// Config variables.
const PUBLIC_ASSETS_PATH = 'public/assets';
const SOURCE_FILES_PATH  = 'src';
const SASS_SRC = SOURCE_FILES_PATH + '/stylesheets/**/*.scss';
const JAVASCRIPTS_SRC = SOURCE_FILES_PATH + '/javascripts/**';
const IMAGES_SRC = SOURCE_FILES_PATH + '/images/**';

var del = require('del');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync  = require('browser-sync');
var webpackCompiler = require('webpack');
var webpack = require('webpack-stream');

gulp.task('browserSync', ['build'], function() {
  browserSync({
    proxy: 'localhost:3000',
    files: ['./app/views/**']
  });
});

var isDevelopment = ['production', 'staging'].indexOf(process.env.RAILS_ENV) === -1;

gulp.task('build', function(cb) {
  var tasks = ['clean', ['copy', 'images', 'js', 'sass']];

  if (!isDevelopment) {
    tasks.push('rev');
  }
  tasks.push(cb);
  $.sequence.apply(this, tasks);
});

gulp.task('clean', function() {
  return del([PUBLIC_ASSETS_PATH]);
});

gulp.task('copy', function() {
  // Copy glyphicon fonts to assets folder.
  const DEST = PUBLIC_ASSETS_PATH + '/fonts/bootstrap';
  return gulp.src('node_modules/bootstrap-sass/assets/fonts/bootstrap/*')
      .pipe(gulp.dest(DEST));
});

var defaultTasks = ['build'];

if (isDevelopment) {
  defaultTasks.push('watch');
}

gulp.task('default', defaultTasks);

gulp.task('js-vendor', function(done) {
  webpackCompiler(require('./webpack.dll.js'), done);
});

gulp.task('js', function() {
  var webpackConfig = require('./webpack.config.js');

  return gulp.src('src/javascripts/application.js')
      .pipe(webpack(webpackConfig, webpackCompiler).on('error', function(err) {
        console.log('Webpack error: ', err);
        this.emit('end');
      })).pipe(gulp.dest(webpackConfig.config[0].output.path));
});

gulp.task('js-watch', ['js'], browserSync.reload);

gulp.task('images', function() {
  const DEST = PUBLIC_ASSETS_PATH + '/images';
  return gulp.src(IMAGES_SRC)
    .pipe($.changed(DEST)) // Ignore unchanged files.
    .pipe($.imagemin())
    .pipe(gulp.dest(DEST))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('rev', ['rev-assets'], function(){
  return gulp.src([PUBLIC_ASSETS_PATH + '/rev-manifest.json', PUBLIC_ASSETS_PATH + '/**/*.{css,js}'])
    .pipe($.revCollector())
    .pipe(gulp.dest(PUBLIC_ASSETS_PATH));
});

gulp.task('rev-assets', function(){
  return gulp.src(PUBLIC_ASSETS_PATH + '/**/*.{css,js,eot,woff,woff2,ttf}')
    .pipe($.rev())
    .pipe(gulp.dest(PUBLIC_ASSETS_PATH))
    .pipe($.rev.manifest())
    .pipe(gulp.dest(PUBLIC_ASSETS_PATH));
});

gulp.task('sass', function() {
  const DEST = PUBLIC_ASSETS_PATH + '/stylesheets';

  return gulp.src(SASS_SRC)
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: ['./node_modules'],
      imagePath: '/assets/images'
    }))
    .pipe($.plumber())
    .pipe($.autoprefixer({ browsers: ['last 2 version'] }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(DEST))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('watch', ['browserSync'], function() {
  gulp.watch(SASS_SRC, ['sass']);
  gulp.watch(IMAGES_SRC, ['images']);
  gulp.watch(JAVASCRIPTS_SRC, ['js-watch']);
});
