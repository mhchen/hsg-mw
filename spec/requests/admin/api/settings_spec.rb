require 'spec_helper'
require 'rails_helper'

describe 'Settings API' do
  let!(:guide) { FactoryGirl.create(:guide1) }
  let!(:admin) { FactoryGirl.create(:admin) }

  before(:each) do
    Setting.admin_email = 'oldsetting'
    Setting.director_email = 'oldsetting'
    Setting.director_name = 'oldsetting'
    Setting.director_title = 'oldsetting'
    Setting.church_website_url = 'oldsetting'
    Setting.church_name = 'oldsetting'
    Setting.church_logo_url = 'oldsetting'
    Setting.email_closing = 'oldsetting'
  end

  it 'should allow updates for admin only' do
    verify_admin_only(guide, admin, '/admin/api/settings', params: {
      settings: {
        admin_email: 'admin@email.com',
        director_email: 'director@email.com',
        director_name: 'Ms. Director',
        director_title: 'Director',
        church_website_url: 'example.com',
        church_name: 'Example Church',
        church_logo_url: 'example.com/example.png',
        email_closing: 'Blah blah blah',
      }
    }, method: :post)

    expect(Setting.admin_email).to eq 'admin@email.com'
    expect(Setting.director_email).to eq 'director@email.com'
    expect(Setting.director_name).to eq 'Ms. Director'
    expect(Setting.director_title).to eq 'Director'
    expect(Setting.church_website_url).to eq 'example.com'
    expect(Setting.church_name).to eq 'Example Church'
    expect(Setting.church_logo_url).to eq 'example.com/example.png'
    expect(Setting.email_closing).to eq 'Blah blah blah'

  end
end
