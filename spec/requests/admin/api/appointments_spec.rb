require 'spec_helper'
require 'rails_helper'

# TODO
# Ordering in DT search
# Searching in DT search

describe 'Appointments API' do
  let(:guide) { FactoryGirl.create(:guide1) }
  let(:guide3) { FactoryGirl.create(:guide3) }
  let(:admin) { FactoryGirl.create(:admin) }

  describe 'table search' do
    let!(:future_appointments) { FactoryGirl.create_list(:registered, 9) }
    let!(:not_own_future_appointment) { FactoryGirl.create(:registered, { guide1_id: guide3.id }) }
    let!(:past_appointments) { FactoryGirl.create_list(:appointment, 10, :in_the_past) }
    let(:dt_params) do
      {
        draw: 1,
        start_date: Time.now.strftime('%Y-%m-%d'),
        columns: {
          '0' => {
            data: 'appointment_slot',
            name: 'appointment_slot',
            searchable: true,
            orderable: true,
            search: {
              value: '',
              regex: false
            }
          },
          '1' => {
            data: 'guide1_name',
            name: 'guide1_id',
            searchable: true,
            orderable: false,
            search: {
              value: '',
              regex: false
            }
          },
          '2' => {
            data: 'location_name',
            name: 'location_id',
            searchable: true,
            orderable: true,
            search: {
              value: '',
              regex: false
            }
          },
          '3' => {
            data: 'type_name',
            name: 'questionnaire_id',
            searchable: true,
            orderable: true,
            search: {
              value: '',
              regex: false
            }
          },
          '4' => {
            data: 'function',
            name: '',
            searchable: false,
            orderable: false,
            search: {
              value: '',
              regex: false
            }
          }
        },
        order: {
          '0' => {
            column: 0,
            dir: 'asc'
          }
        },
        start: 0,
        length: 10,
        search: {
          value: '',
          regex: false
        },
        date: 'today',
      }
    end

    it 'should return a list of appointments for the table' do
      verify_authed_route_and_login(guide, '/admin/api/appointments', dt_params)
      body = json(response.body)
      expect(body['recordsTotal']).to eq(20)
      expect(body['recordsFiltered']).to eq(10)
      data = body['data']
      ids = data.map {|row| row['DT_RowId']}
      future_ids = future_appointments.map(&:id)
      past_ids = past_appointments.map(&:id)
      expect(ids).to include(*future_ids)
      expect(ids).to_not include(*past_ids)
    end

    it 'should display appropriate information for admin and guide' do
      verify_authed_route_and_login(guide, '/admin/api/appointments', dt_params)
      data = json(response.body)['data']
      own_appointment_id = future_appointments.first.id
      not_own_appointment_id = not_own_future_appointment.id

      own_appointment = data.detect {|a| a['DT_RowId'] == own_appointment_id}
      not_own_appointment = data.detect {|a| a['DT_RowId'] == not_own_appointment_id}
      expect(own_appointment['appointment_slot']).to be
      expect(own_appointment['type_name']).to be
      expect(own_appointment['guide1_name']).to be
      expect(own_appointment['guide2_name']).to be
      expect(own_appointment['guide1_id']).to be
      expect(own_appointment['guide2_id']).to be
      expect(own_appointment['member_email']).to be
      expect(own_appointment['member_name']).to be

      expect(not_own_appointment['appointment_slot']).to be
      expect(not_own_appointment['guide1_name']).to be
      expect(not_own_appointment['guide2_name']).to be
      expect(not_own_appointment['guide1_id']).to be
      expect(not_own_appointment['guide2_id']).to be
      expect(not_own_appointment['type_name']).not_to be
      expect(not_own_appointment['member_email']).not_to be
      expect(not_own_appointment['member_name']).not_to be

      login(admin)
      get('/admin/api/appointments', dt_params)
      row = json(response.body)['data'][0]
      expect(row['appointment_slot']).to be
      expect(row['type_name']).to be
      expect(row['guide1_name']).to be
      expect(row['guide2_name']).to be
      expect(row['guide1_id']).to be
      expect(row['guide2_id']).to be
      expect(row['member_email']).to be
      expect(row['member_name']).to be
    end
  end

  describe 'calendar search' do
    before(:each) do
      @last_month = Time.zone.now - 1.month
    end
    let!(:future_appointments) { FactoryGirl.create_list(:registered, 10) }
    let!(:appointment_last_month) do 
      FactoryGirl.create(:appointment, {appointment_slot: @last_month})
    end
    let!(:guide3_appointment_last_month) do 
      FactoryGirl.create(:appointment, :registered, {
        appointment_slot: @last_month,
        guide1: guide3
      })
    end

    it 'should return the proper appointments between start and end' do
      verify_authed_route_and_login(guide3, '/admin/api/appointments/calendar', {
        start_date: @last_month.beginning_of_month.strftime('%Y-%m-%d'),
        end_date: @last_month.end_of_month.strftime('%Y-%m-%d')
      })
      body = json(response.body)
      expect(body.size).to eq(2)
      own_appointment = body.detect {|a| a['id'] == guide3_appointment_last_month.id}
      not_own_appointment = body.detect {|a| a['id'] == appointment_last_month.id}
      expect(own_appointment['belongs_to_guide']).to be true
      expect(own_appointment['is_registered']).to be true
      expect(not_own_appointment['belongs_to_guide']).to be false
      expect(not_own_appointment['is_registered']).to be false
    end

    it 'should return own appointments only if indicated' do
      verify_authed_route_and_login(guide3, '/admin/api/appointments/calendar', {
        start: @last_month.beginning_of_month.strftime('%Y-%m-%d'),
        end: @last_month.end_of_month.strftime('%Y-%m-%d'),
        self_only: true
      })
      body = json(response.body)
      expect(body.size).to eq(1)
      json_appointment = body[0]
      expect(json_appointment['id']).to eq(guide3_appointment_last_month.id)
    end
  end

  describe 'creating new appointments' do
    let(:appointment) { FactoryGirl.build(:appointment) }
    let(:path) { '/admin/api/appointments' }
    let(:appointment_params) do
      {
        guide1_id: appointment.guide1_id,
        guide2_id: appointment.guide2_id,
        location_id: appointment.location_id,
        appointment_slot_time: appointment.appointment_slot_time,
        appointment_slot_date: appointment.appointment_slot_date,
      }
    end

    it 'should restrict to appointments for the logged-in guide only' do
      verify_authed_route_and_login(guide3, path, appointment_params, :post)
      expect(response.status).to eq(401)
      expect(Appointment.count).to eq(0)

      login(guide)
      post path, appointment_params
      expect(response.status).to eq(200)
      expect(Appointment.count).to eq(1)
    end

    it 'should allow admins to add appointments for other people' do
      verify_authed_route_and_login(admin, path, appointment_params, :post)
      expect(response.status).to eq(200)
      expect(Appointment.count).to eq(1)
    end
  end

  describe 'editing appointments' do
    let!(:appointment) { FactoryGirl.create(:appointment) }
    let(:path) { "/admin/api/appointments/#{appointment.id}" }
    let(:new_date) { Date.new(2016, 11, 12) }
    let(:appointment_params) do
      {
        guide1_id: appointment.guide1_id,
        guide2_id: appointment.guide2_id,
        location_id: appointment.location_id,
        appointment_slot_time: appointment.appointment_slot_time,
        appointment_slot_date: '11/12/2012',
      }
    end

    it 'should restrict to appointments for the logged-in guide only' do
      verify_authed_route_and_login(guide3, path, appointment_params, :put)
      expect(response.status).to eq(401)

      login(guide)
      put path, appointment_params
      expect(response.status).to eq(200)
      expect(Appointment.first.appointment_slot_date).to eq('11/12/2012')
    end

    it 'should allow admins to add appointments for other people' do
      verify_authed_route_and_login(admin, path, appointment_params, :put)
      expect(response.status).to eq(200)
      expect(Appointment.first.appointment_slot_date).to eq('11/12/2012')
    end
  end

  describe 'showing appointments' do
    let!(:appointment) { FactoryGirl.create(:appointment) }
    let(:path) { "/admin/api/appointments/#{appointment.id}" }

    it 'should restrict to appointments for the logged-in guide only' do
      verify_authed_route_and_login(guide3, path)
      expect(response.status).to eq(401)

      login(guide)
      get path
      expect(response.status).to eq(200)
    end

    it 'should allow admins to see appointments for other guides' do
      verify_authed_route_and_login(admin, path)
      expect(response.status).to eq(200)
    end
  end

  describe 'deleting appointments' do
    let!(:appointment) { FactoryGirl.create(:appointment) }
    let(:path) { "/admin/api/appointments/#{appointment.id}" }

    it 'should restrict to appointments for the logged-in guide only' do
      verify_authed_route_and_login(guide3, path, {}, :delete)
      expect(response.status).to eq(401)
      expect(Appointment.count).to eq(1)

      login(guide)
      delete path
      expect(response.status).to eq(200)
      expect(Appointment.count).to eq(0)
    end

    it 'should allow admins to see appointments for other guides' do
      verify_authed_route_and_login(admin, path, {}, :delete)
      expect(response.status).to eq(200)
      expect(Appointment.count).to eq(0)
    end
  end
end
