require 'spec_helper'
require 'rails_helper'

describe 'Guides API' do
  let!(:guide) { FactoryGirl.create(:guide1) }
  let!(:admin) { FactoryGirl.create(:admin) }
  let(:new_guide) do
    Guide.new(
      first_name: 'Hulk',
      last_name: 'Hogan',
      email: 'hulk@hogan.com'
    )
  end

  it 'should allow any users to index' do
    verify_authed_route_and_login(guide, '/admin/api/guides')
  end

  describe 'creating new guides' do
    before(:each) do
      delivery = double
      allow(delivery).to receive(:deliver_now).with(no_args)
      allow(GuidesMailer).to receive(:welcome_email).and_return(delivery)
    end
    
    it 'should allow admin to create a new guide and send a signup email' do
      old_count = Guide.count
      verify_admin_only(guide, admin, '/admin/api/guides', params: new_guide.serializable_hash, method: :post)
      expect(Guide.count).to eq(old_count + 1)

      guide_from_db = Guide.find_by(email: 'hulk@hogan.com')
      expect(guide_from_db).to be

      expect(GuidesMailer).to have_received(:welcome_email).with(guide_from_db)
    end

    it 'should not send an email if no_email param is set' do
      old_count = Guide.count
      params = new_guide.serializable_hash.merge({
        no_email: true
      })
      verify_admin_only(guide, admin, '/admin/api/guides', params: params, method: :post)
      expect(Guide.count).to eq(old_count + 1)

      guide_from_db = Guide.find_by(email: 'hulk@hogan.com')
      expect(guide_from_db).to be

      expect(GuidesMailer).to_not have_received(:welcome_email)
    end
  end

  it 'should allow admin to update a guide' do
    new_guide.password = 'random'
    new_guide.save!
    old_password = new_guide.encrypted_password
    verify_admin_only(guide, admin, "/admin/api/guides/#{new_guide.id}", params: {
      first_name: 'Hulk',
      last_name: 'Hogan',
      email: 'hulk@hogan.net',
    }, method: :put)
    expect(Guide.find_by(email: 'hulk@hogan.com')).to_not be

    guide_from_db = Guide.find_by(email: 'hulk@hogan.net')
    expect(guide_from_db).to be

    # It should not change the password.
    expect(guide_from_db.encrypted_password).to eq old_password
  end

  it 'should allow admin to delete a guide' do
    new_guide.password = 'random'
    new_guide.save!
    old_count = Guide.count
    verify_admin_only(guide, admin, "/admin/api/guides/#{new_guide.id}", method: :delete)
    expect(Guide.count).to eq(old_count - 1)
    expect(Guide.find_by(email: 'hulk@hogan.com')).to_not be
  end

  it 'should allow any guide to update their own information' do
    login(guide)
    post '/admin/api/guides/me', {
      first_name: 'NewFirst',
      last_name: 'NewLast',
      email: 'new@email.com',
    }

    guide.reload

    expect(guide.first_name).to eq('NewFirst')
    expect(guide.last_name).to eq('NewLast')
    expect(guide.email).to eq('new@email.com')
  end
end
