module Requests
  module JsonHelpers
    def verify_admin_only(guide, admin, path, params: {}, method: :get)
      login(guide)
      send(method, path, params)

      expect(response).to have_http_status(:unauthorized)

      login(admin)
      send(method, path, params)
      expect(response).to have_http_status(:success)
    end

    def verify_authed_route_and_login(guide, path, params = {}, method = :get)
      send(method, path, params)
      expect(response).to have_http_status(:unauthorized)

      login(guide)

      send(method, path, params)
    end

    def login(guide)
      post '/admin/api/guides/sessions/sign_in', {
        email: guide.email,
        password: 'abcdef'
      }
      raise response.body if response.status != 200
    end

    def json(body)
      JSON.parse(body)
    end
  end
end
