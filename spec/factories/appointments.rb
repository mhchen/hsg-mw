FactoryGirl.define do
  factory :appointment do
    guide1
    guide2
    location
    appointment_slot { Faker::Time.forward(60) }

    trait :in_the_past do
      appointment_slot { Faker::Time.backward(60) }
    end

    trait :registered do
      after(:build) do |appointment|
        appointment.registration ||= FactoryGirl.build(:registration, appointment: appointment)
      end
    end

    factory :registered, traits: [:registered]
  end
end
