FactoryGirl.define do
  factory :registration do
    member_name 'Testy McTesterson'
    member_email 'testy@test.com'
    appointment_type {
      AppointmentType.first || association(:appointment_type)
    }
  end
end
