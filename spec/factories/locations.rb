FactoryGirl.define do
  factory :location do
    name 'Green Room'
    description '735, nowhere in particular'
  end

  factory :office do
    name "Jack's Office"
    description '735, down the hall to the left'
  end
end
