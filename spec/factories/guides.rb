FactoryGirl.define do
  factory :guide1, class: Guide do
    id 1
    first_name 'Mike'
    last_name 'Smith'
    email 'mike@smith.net'
    password 'abcdef'
    initialize_with { Guide.find_or_create_by(id: id) }
  end

  factory :guide2, class: Guide do
    id 2
    first_name 'Joe'
    last_name 'Bazooka'
    email 'joe@bazooka.org'
    password 'abcdef'
    initialize_with { Guide.find_or_create_by(id: id) }
  end

  factory :guide3, class: Guide do
    id 4
    first_name 'John'
    last_name 'Jacob'
    email 'jingle@heimerschmit.com'
    password 'abcdef'
    initialize_with { Guide.find_or_create_by(id: id) }
  end

  factory :admin, class: Guide do
    id 4
    first_name 'Admin'
    last_name 'Adminson'
    email 'admin@highrock.org'
    password 'abcdef'
    is_admin true
    initialize_with { Guide.find_or_create_by(id: id) }
  end
end
