FactoryGirl.define do
  factory :questionnaire do
    initialize_with { new(FactoryGirl.create(:registered)) }
    skip_create
  end

  # TODO: Pretty gross pattern here. See https://github.com/thoughtbot/factory_girl/issues/683
  # and attempt to clean up at some point
  factory :question_category do
    text "Connecting to God's people"
    sort 10
    appointment_type {
      AppointmentType.first || association(:appointment_type)
    }
  end

  factory :question_category2, class: QuestionCategory do
    text "Connecting to God's purposes"
    sort 20
    appointment_type {
      AppointmentType.first || association(:appointment_type)
    }
  end

  factory :question do
    text 'How are you?'
    sort 10
    question_category {
      QuestionCategory.first || association(:question_category)
    }
  end

  factory :question2, class: Question do
    text 'Everything okay?'
    sort 20
    question_category {
      QuestionCategory.last || association(:question_category)
    }
  end
end
