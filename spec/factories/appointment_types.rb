FactoryGirl.define do
  factory :appointment_type do
    name 'New Member'
    slug 'new-member'
    questionnaire_description 'the new member questionnaire'
    description 'new member interview appointment with the Highrock Spiritual Guides'
  end
end
