require "rails_helper"

RSpec.describe GuidesMailer, type: :mailer do
  before(:all) do
    Setting.admin_email = 'admin@highrock.org'
    Setting.director_email = 'director@highrock.org'
    Setting.director_name = 'Ms. Director'
    Setting.director_title = 'Director Person'
    Setting.church_website_url = 'http://highrock.org'
  end

  let (:appointment) { FactoryGirl.create :registered }
  let (:questionnaire) { FactoryGirl.create :questionnaire }

  it 'should send registration emails' do
    mail = described_class.registration_email(appointment).deliver_now
    expect(mail.from[0]).to eq('director@highrock.org')
    expect(mail.to[0]).to eq('testy@test.com')

    due_date_str = (appointment.appointment_slot - 2.day).strftime('%-m/%-d')
    expect(mail.subject).to include(due_date_str)
    expect(mail.body).to include('Thank you for signing up for your new member interview appointment')
    expect(mail.body).to include('the Green Room')
    expect(mail.body).to include('735, nowhere in particular')
    expect(mail.body).to include('the new member questionnaire')
    expect(mail.body).to include('Ms. Director')
    expect(mail.body).to include('Director Person')
    expect(mail.body).to include('http://highrock.org')
    expect(mail.cc).to include('joe@bazooka.org', 'mike@smith.net')
    expect(mail.bcc[0]).to eq('admin@highrock.org')
  end

  it 'should send questionnaire emails' do
    FactoryGirl.create(:question_category)
    FactoryGirl.create(:question_category2)
    question = FactoryGirl.create(:question)
    question2 = FactoryGirl.create(:question2)

    questionnaire.send("question_#{question.id}=", 'Good!')
    questionnaire.send("question_#{question2.id}=", 'Yup!')

    mail = described_class.questionnaire_email(questionnaire).deliver_now

    expect(mail.body).to include('How are you?', 'Everything okay?', 'Good!', 'Yup!')
    expect(mail.from[0]).to eq('director@highrock.org')
    expect(mail.to[0]).to eq('testy@test.com')
    expect(mail.cc).to include('joe@bazooka.org', 'mike@smith.net')
    expect(mail.bcc).to be_falsey
  end

  it 'should send welcome emails' do
    guide = FactoryGirl.build(:guide1)
    guide = Guide.create_with_initial_password(guide.serializable_hash)
    mail = described_class.welcome_email(guide).deliver_now
    expect(mail.to[0]).to eq('mike@smith.net')
    expect(mail.body).to include(guide.password)
  end

  it 'should raise if password is nil or the guide isn\'t persisted' do
    guide = FactoryGirl.create(:guide1)
    guide = Guide.find(guide.id) # Password won't be filled in.
    expect{described_class.welcome_email(guide).deliver_now}.to raise_error(GuidesMailer::InvalidGuideError)

    guide = FactoryGirl.build(:guide2)
    expect{described_class.welcome_email(guide).deliver_now}.to raise_error(GuidesMailer::InvalidGuideError)
    
  end

  it 'should send cancellation emails' do
    mail = described_class.cancellation_email(appointment).deliver_now
    expect(mail.body).to include('guides.highrock.org')
    expect(mail.body).to include('Ms. Director')
    expect(mail.body).to include('Director Person')
    expect(mail.body).to include('http://highrock.org')
    expect(mail.from[0]).to eq('director@highrock.org')
    expect(mail.to[0]).to eq('testy@test.com')
    expect(mail.cc).to include('joe@bazooka.org', 'mike@smith.net')
    expect(mail.bcc[0]).to eq('admin@highrock.org')
  end
end
