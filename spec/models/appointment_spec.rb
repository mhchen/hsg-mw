require 'spec_helper'
require 'rails_helper'

RSpec.describe Appointment, type: :model do

  describe '#assign_location' do
    before(:each) do
      Location.create(name: 'First location', description: 'My first location')
      Location.create(name: 'Second location', description: 'My second location')
      Location.create(name: 'Third location', description: 'My third location')
      Location.create(name: 'fourth location', description: 'My fourth location')

      # Create guides for validation purposes.
      @guide1 = Guide.create!(
        first_name: 'Test',
        last_name: 'Blah',
        email: 'test@blah.com',
        password: 'abcdef'
      )
      @guide2 = Guide.create!(
        first_name: 'Test2',
        last_name: 'Blah2',
        email: 'test2@blah.com',
        password: 'abcdef'
      )
      @locations = Location.all
    end

    it 'should leave the location_id alone if already assigned' do
      appointment = Appointment.new
      appointment.location_id = @locations[3].id
      appointment.assign_location
      expect(appointment.location_id).to eq(@locations[3].id)
    end

    it 'should assign with the first location_id if there are no appointments near it' do
      appointment = Appointment.new
      appointment.appointment_slot = Time.now
      appointment.assign_location
      expect(appointment.location_id).to eq(@locations[0].id)
    end

    it 'should assign a free location_id if there are conflicts' do
      now = Time.now
      appointment = Appointment.new
      appointment.appointment_slot = now
      create_appointment(
        appointment_slot: now - 89.minute,
        location_id: @locations[0].id)
      create_appointment(
        appointment_slot: now + 89.minute,
        location_id: @locations[1].id)
      create_appointment(
        appointment_slot: now + 45.minute,
        location_id: @locations[3].id)
      appointment.assign_location
      expect(appointment.location_id).to eq(@locations[2].id)
    end

    it 'should be unaffected by appointments that are not nearby' do
      now = Time.now
      appointment = Appointment.new
      appointment.appointment_slot = now
      create_appointment(appointment_slot: now - 89.minute, location_id: @locations[0].id)
      create_appointment(appointment_slot: now - 91.minute, location_id: @locations[1].id)
      appointment.assign_location
      expect(appointment.location_id).to eq(@locations[1].id)
    end

    it 'should assign the first location if all locations are taken' do
      now = Time.now
      appointment = Appointment.new
      appointment.appointment_slot = now
      Appointment.create(appointment_slot: now - 89.minute, location_id: @locations[0].id)
      Appointment.create(appointment_slot: now + 89.minute, location_id: @locations[1].id)
      Appointment.create(appointment_slot: now + 45.minute, location_id: @locations[2].id)
      Appointment.create(appointment_slot: now - 45.minute, location_id: @locations[3].id)
      appointment.assign_location
      expect(appointment.location_id).to eq(@locations[0].id)
    end
  end

  # Creates an appointment with guides assigned (for validation purposes).
  def create_appointment(params)
    Appointment.create!(params.merge({
      guide1: @guide1,
      guide2: @guide2
    }))
  end
end
