require 'rails_helper'

RSpec.describe Registration, type: :model do
  let!(:appointment) { FactoryGirl.create(:registered) }

  it 'should be deleted when its appointment is deleted' do
    expect(Registration.count).to eq(1)
    appointment.destroy
    expect(Registration.count).to eq(0)
  end

  describe 'validations' do
    it 'should only allow one registration per appointment' do
      new_registration = Registration.new(
        appointment: appointment,
        member_name: 'Bloop Blooperson',
        member_email: 'bloop@bloop.com',
        appointment_type: AppointmentType.first
      )
      expect{new_registration.save!}.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'should require a valid appointment' do
      new_registration = Registration.new(
        appointment_id: -1,
        member_name: 'Bloop Blooperson',
        member_email: 'bloop@bloop.com',
        appointment_type: AppointmentType.first
      )
      expect{new_registration.save!}.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'should only allow valid member emails' do
      registration = appointment.registration
      registration.member_email = 'bloop'
      expect{registration.save!}.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
