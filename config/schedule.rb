set :output, {:error => 'log/cron-error.log', :standard => 'log/cron.log'}

every 1.day, at: '5 pm' do
  rake 'reminder_emails'
end
