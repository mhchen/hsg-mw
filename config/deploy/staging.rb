set :deploy_to, '/home/hsg/staging'
set :rails_env, 'staging'
server 'guides.highrock.org', roles: %w(web app db), primary: true, user: 'hsg'
