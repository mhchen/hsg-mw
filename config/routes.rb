Rails.application.routes.draw do
  devise_for :guides, path: '/admin/api/guides/sessions', controllers: {
    sessions: 'admin/api/guides/sessions'
  }

  get 'appointments/index'

  root to: 'appointments#index'

  resources :appointments, only: [:edit, :update]

  get '/thank-you', to: 'appointments#thank_you'

  get '/questionnaire/thank-you', to: 'questionnaires#thank_you'
  get '/questionnaire/:instance_id', to: 'questionnaires#show'
  post '/questionnaire/:instance_id', to: 'questionnaires#respond'


  namespace :admin do
    get '/appointments/csv', to: 'appointments#csv'
    namespace :api do
      devise_scope :guide do
        get '/guides/sessions/current-user', to: 'guides/sessions#current_user', as: 'current_user'
      end

      post '/guides/reset-password', to: 'guides/passwords#reset_password'
      get '/guides/from-token', to: 'guides/passwords#from_token'
      post '/guides/new-password', to: 'guides/passwords#new_password'

      get '/guides/count-dependent-appointments', to: 'guides#count_dependent_appointments'
      resources :guides
      post '/guides/me', to: 'guides#update_me'
      get '/appointments/calendar', to: 'appointments#calendar'
      resources :appointments
      resources :locations, only: :index
      resources :appointment_types, only: [:index, :show, :update]
      resources :settings, only: [:index]
      post '/settings', to: 'settings#mass_update'
      resources :locations, only: [:index]
      get '/locations/count-dependent-appointments', to: 'locations#count_dependent_appointments'
      post '/locations', to: 'locations#mass_update'
    end
    get '/', to: 'admin#index'
    get '*path', to: 'admin#index'
  end
end
