module ActiveRecord
  class Relation
    def to_hash
      self.each_with_object({}) do |obj, hash|
        hash[obj.id] = obj
      end
    end
  end
end
