# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'hsg'
set :repo_url, 'git@gitlab.com:mhchen/hsg.git'

# Default branch is :master
set :branch, 'master'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, 'config/database.yml', 'config/secrets.yml'

# Default value for linked_dirs is []
# append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'

# Default value for default_env is {}
 set :default_env, { rvm_bin_path: '~/.rvm/bin' }

# Default value for keep_releases is 5
 set :keep_releases, 3
