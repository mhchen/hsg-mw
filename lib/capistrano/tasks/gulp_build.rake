namespace :deploy do
  desc 'Installs npm packages and builds assets'
  task :gulp_build do
    run_locally do
      execute "yarn && RAILS_ENV=production gulp build"
    end
  end

  task :gulp_upload do
    invoke 'deploy:gulp_build'

    on roles(:web) do
      upload!(File.join(Dir.pwd, 'public', 'assets','.'), release_path.join('public/assets/'), recursive: true)
    end
  end

  after 'deploy:compile_assets', 'deploy:gulp_upload'
end
