namespace :git do
  desc 'Checks for diffs between local and origin/master'
  task :check_dirty do
    run_locally do
      execute 'git fetch'
      data = capture 'git diff --word-diff=porcelain --name-status origin/master'
      abort "Not synced with origin/master:\n#{data}" if data.size > 0
    end
  end

  before 'deploy:starting', 'git:check_dirty'
end
