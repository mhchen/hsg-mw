task reminder_emails: :environment do
  REMINDER_EMAIL_PERIOD = 3.days

  NOW = Time.zone.now

  puts "Starting reminder emails for #{NOW.strftime('%F')}"
  appointments = Appointment.needs_reminder_email(NOW + REMINDER_EMAIL_PERIOD)
  not_sending_appointments = Appointment.needs_reminder_email(NOW + REMINDER_EMAIL_PERIOD - 1.day, NOW)
  if not_sending_appointments.present?
    puts "Warning: found appointments with reminder emails that will not be sent: #{not_sending_appointments.map(&:id)}"
  end

  if appointments.empty?
    puts 'No emails found!'
  else
    puts "Found appointments that need reminders: #{appointments.map(&:id)}"
    appointments.each do |appointment|
      GuidesMailer.reminder_email(appointment).deliver_now
      puts "Sending mail for appointment ID: #{appointment.id}"
      appointment.update_reminder_email_sent(NOW)
    end
  end
end
