task import_mysql: [:environment, 'db:drop', 'db:create', 'db:schema:load'] do
  require 'mysql2'
  require 'htmlentities'
  client = Mysql2::Client.new(
    :host => ENV['HSG_MYSQL_IP'],
    :username => '397629_highrock',
    :database => '397629_highrock',
    :password => ENV['HSG_MYSQL_PASSWORD']
  )

  puts "==== Importing guides ===="
  client.query("SELECT * FROM guide", symbolize_keys: true).each do |guide|
    next unless guide[:is_active]
    puts "Importing guide #{guide[:first_name]} #{guide[:last_name]}"
    Guide.create!({
      id: guide[:id],
      first_name: guide[:first_name],
      last_name: guide[:last_name],
      email: guide[:email],
      password: 'abcdef'
    })
  end

  puts "==== Importing locations ===="
  client.query("SELECT * FROM location", symbolize_keys: true).each do |location|
    next unless location[:is_active]
    puts "Importing #{location[:name]}"
    Location.create({
      id: location[:id],
      name: location[:name],
      description: location[:description],
    })
  end

  puts "==== Importing meeting types ===="
  client.query("SELECT * FROM questionnaire", symbolize_keys: true).each do |questionnaire|
    puts "Importing #{questionnaire[:name]}"
    case questionnaire[:name]
    when 'New member'
      description = 'new member interview appointment with the Highrock Spiritual Guides';
      questionnaire_description = 'the new member questionnaire';
    when 'Membership renewal'
      description = 'Highrock Spiritual Guides membership renewal appointment';
      questionnaire_description = 'the member renewal questionnaire';
    when 'Follow-up Meeting'
      description = 'appointment with the Highrock Spiritual Guides';
      questionnaire_description = 'a brief survey to prepare for your appointment';
    end
    AppointmentType.create({
      id: questionnaire[:id],
      name: questionnaire[:name],
      slug: questionnaire[:url_path].sub(/^\//, '').sub(/-(survey|questionnaire)$/, ''),
      description: description,
      questionnaire_description: questionnaire_description
    })
  end

  puts "==== Importing appointments ===="
  client.query("SELECT * FROM appointment", symbolize_keys: true).each do |appointment|
    next if appointment[:questionnaire_id] == 0 && (appointment[:member_email] != '' || appointment[:member_name] != '')
    puts "Importing appointment at #{appointment[:appointment_slot]}"
    a = Appointment.create({
      id: appointment[:id],
      guide1_id: appointment[:guide1_id] == -1 ? nil : appointment[:guide1_id],
      guide2_id: appointment[:guide2_id] == -1 ? nil : appointment[:guide2_id],
      # Convert to UTC.
      appointment_slot: ActiveSupport::TimeZone['US/Eastern'].parse(appointment[:appointment_slot].asctime),
      location_id: appointment[:location_id],
      member_name: appointment[:member_name],
      member_email: appointment[:member_email],
      appointment_type_id: appointment[:questionnaire_id],
      survey_done: appointment[:survey_done],
      reminder_email_sent: appointment[:reminder_email_sent] || false
    })
    if a.member_name
      a.generate_questionnaire_instance_id
    end
    a.save
  end

  puts "==== Importing questions/categories ===="
  coder = HTMLEntities.new
  categories = {}
  client.query("SELECT * FROM question JOIN question_category ON question.question_category_id = question_category.id", symbolize_keys: true).each do |question|
    category_name = question[:name]
    category_id = "#{question[:questionnaire_id]}-#{category_name}"
    if categories[category_id]
      category = categories[category_id]
    else
      category = QuestionCategory.new
      if category_name.include? '<span'
        category.instructions = /\((.*)\)/.match(category_name)[1]
        category_name = category_name.sub(/ \<span.*>/, '')
      end
      category.text = category_name
      category.appointment_type_id = question[:questionnaire_id]
      categories[category_id] = category
      category.sort = question[:weight]
      category.save!
    end
    new_question = Question.new
    new_question.question_category = category
    new_question.text = coder.decode(question[:text])
    new_question.sort = question[:sort]
    new_question.save!
  end

  # Reset PKs.
  ActiveRecord::Base.connection.tables.each do |t|
    ActiveRecord::Base.connection.reset_pk_sequence!(t)
  end
end
