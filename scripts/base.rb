AppointmentType.create!([
  {name: "New member", slug: "new-member", questionnaire_description: "the new member questionnaire", description: "new member interview appointment with the Highrock Spiritual Guides"},
  {name: "Membership renewal", slug: "renewal", questionnaire_description: "the member renewal questionnaire", description: "Highrock Spiritual Guides membership renewal appointment"},
  {name: "Follow-up Meeting", slug: "follow-up", questionnaire_description: "a brief survey to prepare for your appointment", description: "appointment with the Highrock Spiritual Guides"}
])

Location.create!([
  {name: "Highrock Spiritual Guides Room (Mill St Meeting Room A)", description: "Mill Street, the first room on your left after going through the single door to the hallway", sort: 10},
  {name: "Mill St Meeting Room B", description: "Mill Street, far end of the hallway next to the kitchen with the very large window.", sort: 10},
  {name: "Pastor Eugene's Office", description: "Mill Street, third door on your right after entering the building", sort: 10},
  {name: "Shawn Houten's Office", description: "Mill Street, first door on your right after entering the building", sort: 10}
])

QuestionCategory.create!([
  {text: "Your Life Highlights", sort: 5, appointment_type_id: 1, instructions: "Please do not take more than one paragraph to answer each question."},
  {text: "Preparing For Your Meeting", sort: 5, appointment_type_id: 3, instructions: nil},
  {text: "Overview of Past Year", sort: 5, appointment_type_id: 2, instructions: "Please answer each question with one paragraph or less."},
  {text: "Connecting to God personally", sort: 10, appointment_type_id: 1, instructions: nil},
  {text: "Connecting to God personally", sort: 10, appointment_type_id: 2, instructions: nil},
  {text: "Connecting to God's people", sort: 20, appointment_type_id: 1, instructions: nil},
  {text: "Connecting to God's people", sort: 20, appointment_type_id: 2, instructions: nil},
  {text: "Connecting to God's purposes", sort: 30, appointment_type_id: 1, instructions: nil},
  {text: "Connecting to God's purposes", sort: 30, appointment_type_id: 2, instructions: nil}
])


Question.create!([
  {text: "What have been the greatest highlights and lowlights of the past year?", sort: 1, question_category_id: 8},
  {text: "Describe an experience where God transformed your heart, mind, or soul this past year.", sort: 2, question_category_id: 8},
  {text: "How would you describe your relationship with God at present? How might God be inviting you to grow deeper in relationship with him over the next year?", sort: 10, question_category_id: 1},
  {text: "Please give us a brief overview of what you would like to discuss during this meeting (2-3 sentences is fine).", sort: 10, question_category_id: 9},
  {text: "Describe the ways that you most deeply connect with God. Are you able to incorporate these into your life?", sort: 20, question_category_id: 1},
  {text: "Does the content of this meeting relate to a previous meeting you have had with the guides?  If so, in what way?", sort: 20, question_category_id: 9},
  {text: "Is there anything else you would like to share before meeting?", sort: 30, question_category_id: 9},
  {text: "Are there any sins you are struggling with right now that you would like to share? We would love to pray for you, encourage you, and recommend resources. (If you like you may leave this blank and/or discuss during your personal meeting.)", sort: 30, question_category_id: 1},
  {text: "God intends for the Sabbath to be a restful and life-giving practice for us. How do you observe the Sabbath?", sort: 40, question_category_id: 1},
  {text: "Is there any other area you would like to discuss regarding your spiritual life right now?", sort: 50, question_category_id: 1},
  {text: "Are there people in your life with whom you are able to be honest about your beliefs, feelings, doubts, and hurts beneath the surface of your life? How might God be inviting you to connect with others more deeply this year?", sort: 60, question_category_id: 2},
  {text: "Are you able to resolve conflict in a clear, direct, and respectful way? Where do you need to grow in this area?", sort: 70, question_category_id: 2},
  {text: "Are you currently having any relational difficulties with anyone — Christian or non-Christian? If so, please briefly comment. How might God be inviting you to grow through this conflict? (If you like you may leave this blank and/or discuss during your personal meeting.)", sort: 80, question_category_id: 2},
  {text: "In what ways are you currently connecting to God's purposes? Where do you think God might be inviting you to grow in this area over the next year?", sort: 90, question_category_id: 3},
  {text: "“Our passion is meant to be expressed in and for community.” (Dan Allender). What do you most enjoy doing? Do you have any passions that have emerged out of suffering? How might God be inviting you to bless the church and/or the world with your passions?", sort: 100, question_category_id: 3},
  {text: "Do you currently worship God through tithing? Are you able to trust God with your finances? Are you in significant consumer debt? Where can you grow in generosity of your time, treasure, and talents?", sort: 110, question_category_id: 3},
  {text: "Please share your personal testimony briefly.", sort: 120, question_category_id: 4},
  {text: "What have been the greatest highlights of your life so far?", sort: 130, question_category_id: 4},
  {text: "What have been the most significant lowlights of your life so far?", sort: 140, question_category_id: 4},
  {text: "In what ways are you currently connecting to God spiritually? Where do you think God might be inviting you to grow in this area over the next year?", sort: 150, question_category_id: 5},
  {text: "Are there any sins that you are struggling with right now that you would like to share? (If you like you may leave this blank and/or discuss during your personal meeting.)", sort: 160, question_category_id: 5},
  {text: "Do you currently worship God through tithing? If yes, how has tithing impacted your relationship with God? If not, what prevents you from doing so?", sort: 170, question_category_id: 5},
  {text: "Is there any other area you would like to discuss regarding your spiritual life right now?", sort: 180, question_category_id: 5},
  {text: "In what ways are you currently connecting to God's people — through small groups or otherwise? Where do you think God might be inviting you to grow in this area over the next year?", sort: 190, question_category_id: 6},
  {text: "Are you currently having any relational difficulties with anyone — Christian or non-Christian? If so, please briefly comment. (If you like you may leave this blank and/or discuss during your personal meeting.)", sort: 200, question_category_id: 6},
  {text: "In what ways are you currently connecting to God's purposes? Where do you think God might be inviting you to grow in this area over the next year?", sort: 210, question_category_id: 7},
  {text: "Do you feel confident about sharing your faith with nonbelievers? How are you currently doing so? What do you think would help you to share more freely?", sort: 220, question_category_id: 7},
  {text: "How has your experience been at Highrock? Would you like to share any constructive feedback about the church with us?", sort: 230, question_category_id: 7}
])
