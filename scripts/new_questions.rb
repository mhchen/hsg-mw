Question.delete_all
QuestionCategory.delete_all
at = AppointmentType.find_by(slug: 'spiritual-guides')
qc = QuestionCategory.new
qc.text = 'Preparing For Your Meeting'
qc.instructions = 'Ponder these questions before God as you prepare for your appointment.  One or two of them may seem to fit your circumstances at this moment in time.  Bring those questions with you into the spiritual conversation.'
qc.sort = 10
at.question_categories << qc

sort = 10

[
  'What are you seeking from God for your life?',
  'What is happening in your prayer life?',
  'How has your image of God changed over the years?  Who is God to you today?',
  'Where is it easiest (and/or most difficult) for you to connect with God?',
  'Do you sense that God is inviting you into something new or different?',
  'Are you facing a choice that requires spiritual discernment?  How are you listening to God about your choice?',
  'What spiritual practices sustain you on a daily or monthly basis?',
].each do |text|
  q = Question.create({
    text: text,
    sort: sort,
  })
  qc.questions << q
  sort += 10
end

AppointmentType.where.not(slug: 'spiritual-guides').each do |at|
  qc = QuestionCategory.new
  qc.text = 'Connecting to God Personally'
  qc.sort = 10
  at.question_categories << qc

  qc = QuestionCategory.new
  qc.text = 'Connecting to God\'s People'
  qc.sort = 20
  at.question_categories << qc

  qc = QuestionCategory.new
  qc.text = 'Connecting to God\'s Purposes'
  qc.sort = 30
  at.question_categories << qc

  qc = QuestionCategory.new
  qc.text = 'Summary'
  qc.sort = 40
  at.question_categories << qc

  '(New Member) Please share your journey of coming to faith in Jesus.'
  'Where do you connect with or struggle to connect with God (e.g., worship service, small group, Bible reading, prayer, other)?'
  'Are there any areas of struggle you wish to discuss?'
  'Connecting to God’s People'
  'Where and how are you connecting with God’s people? Or where do you have difficulties connecting with others?'
  'Are there any broken relationships in your life that invite repentance, forgiveness, or reconciliation?  Explain.'
  'Connecting to God’s Purposes'
  'Tell us about your efforts to practice faithful stewardship with your finances, talents, and time to pursue God\'s purposes in this world.' 
  'How have you been sharing your spiritual journey or witnessing with others?' 
  'Are you involved in any ministries at Highrock? Tell us about your experience and what you are learning about your gifts.'
  'Summary'
  'How do you sense that you are growing in your love for God and neighbor this year?'
  'Is there anything else you would like to share with us about your spiritual journey or your experience at Highrock?' 
  '(New Member) Do you have any questions about Highrock or are there ministries that you would like to be connected with?'
end
