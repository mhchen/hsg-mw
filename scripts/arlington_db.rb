# Settings for the Arlington church.
Setting.mass_update({
  admin_email: 'abby@highrock.org',
  director_email: 'amy@highrock.org',
  director_name: 'Amy Bositis',
  director_title: 'Director of Spiritual Formation',
  church_website_url: 'http://www.highrock.org',
  church_name: 'Highrock Covenant Church',
  church_logo_url: 'http://www.highrock.org/_img/highrock-arlington-logo-transparent-background.png',
  email_closing: 'Grace & Peace',
  prepare_url: 'http://www.highrock.org/spiritualguidesprepare/',
  ministry_description_url: 'http://www.highrock.org/spiritualguides/',
})
