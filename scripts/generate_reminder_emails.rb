exit unless Rails.env == 'development'

guides_counter = 0
guides = Guide.all

AppointmentType.all.each do |type|
  appointment = Appointment.new
  appointment.guide1 = guides[guides_counter]
  guides_counter += 1
  appointment.guide2 = guides[guides_counter]
  guides_counter += 1
  appointment.location = Location.first
  appointment.appointment_slot = Time.now + 3.day
  appointment.save!
  appointment.create_registration!({
    member_name: 'Testy testerson',
    member_email: 'test@test.com',
    appointment_type_id: type.id
  })
end
