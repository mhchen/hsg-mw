AppointmentType.where(slug: 'follow-up').update_all(is_active: false)

AppointmentType.find_by(slug: 'new-member').update(
  description: 'new member Highrock Spiritual Guides appointment'
)

AppointmentType.find_by(slug: 'renewal').update(
  description: 'Highrock Spiritual Guides membership renewal appointment'
)

AppointmentType.create!([{
  name: 'Spiritual Guides',
  slug: 'spiritual-guides',
  questionnaire_description: 'the Highrock Spiritual Guides questionnaire',
  description: 'Highrock Spiritual Guides meeting'
}])

