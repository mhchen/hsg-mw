var path = require('path');
var webpack = require('webpack');

var srcDir = path.join(__dirname, 'src', 'javascripts');
var destDir = path.join(__dirname, 'public', 'assets', 'javascripts');

module.exports = {
  entry: {
    vendor: ['./src/javascripts/vendor.js']
  },
  output: {
    path: destDir,
    filename: 'dll.[name].js',
    library: '[name]'
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      'window.$': 'jquery'
    }),
    new webpack.DllPlugin({
      path: path.resolve(__dirname, './tmp/dll/[name]-manifest.json'),
      name: '[name]'
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin()
  ]
};
